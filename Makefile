DESTDIR ?= /opt/gasetup

.PHONY: install
install:
	install -d $(DESTDIR)
	install -d $(DESTDIR)/core
	install -d $(DESTDIR)/core/configs
	install -d $(DESTDIR)/core/libs
	install -d $(DESTDIR)/core/procedures
	install -d $(DESTDIR)/core/uis
	install -d $(DESTDIR)/user

	install -m 755 gasetup.sh $(DESTDIR)
	install -m 755 libui.sh $(DESTDIR)
	install -m 777 login.sh $(DESTDIR)

	install -m 644 user/whatsthis.txt $(DESTDIR)/user

	install -m 755 core/libs/*.sh $(DESTDIR)/core/libs
	install -m 755 core/uis/*.sh $(DESTDIR)/core/uis

	install -m 755 core/procedures/base $(DESTDIR)/core/procedures
	install -m 755 core/procedures/interactive $(DESTDIR)/core/procedures

	install -m 644 gasetup.desktop /usr/share/applications
	install -m 644 gasetup.png /usr/share/pixmaps
	install -m 644 groovyframe-wallpaper.png $(DESTDIR)

	cp -r core/configs $(DESTDIR)/core/
.PHONY: uninstall
uninstall:
	rm -rf $(DESTDIR)
	rm /usr/share/applications/gasetup.desktop
	rm /usr/share/pixmaps/gasetup.png
