#!/bin/bash

configure() {
  dest="$GA_FRONTENDS"/attract
  su $GA_USER <<EOF
    install -dm775 "$dest"
    cp attract.cfg "$dest"
    ln -s "$dest" "$GA_HOME"/.attract
EOF
}

update() {
  local v upgrades=(
    3.0.0-1
  )

  for v in "${upgrades[@]}"; do
    if [[ $(vercmp "$v" "$1") -eq 1 ]]; then
      log_info "Proceeding attract upgrade to $2"
      "_${v//[.-]/_}_changes"
    fi
  done
}

_3_0_0_1_changes() {
  # Clear default_font and font_path in default $GA_USER
  su $GA_USER <<EOF
    shopt -s extglob
    cp -r /usr/local/share/attract/!(emulators) "$GA_FRONTENDS"/attract/
EOF
  for p in default_font font_path ; do
    sed -E -i "/[[:space:]]+$p[[:space:]]+.*/d" "$GA_FRONTENDS"/attract/attract.cfg
  done
}