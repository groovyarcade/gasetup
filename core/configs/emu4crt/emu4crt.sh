#!/bin/bash
local systems="sms gamegear megadrive nes snes gb gbc pcengine ngpc apple2 lynx wonderswan psx pcfx saturn"

configure() {
	set -x
  su $GA_USER <<EOF
for s in $systems ; do
mkdir -p "$GA_ROMS"/"$s"
done

mkdir -p "$GA_CONFIGS"/emu4crt "$MOTHER_OF_ALL"/bios

ln -s shared/configs/emu4crt "$GA_HOME"/.mednafen
ln -s "$MOTHER_OF_ALL"/bios "$GA_HOME"/.mednafen/firmware
EOF

cp mednafen.cfg "$GA_CONFIGS"/emu4crt
chown $GA_USER:$GA_GROUP "$GA_CONFIGS"/emu4crt/mednafen.cfg

export GA_USER GA_GROUP GA_FRONTENDS
attract_config
#emulationstation_config
set -x
}


update() {
  return 0
}


attract_config() {
  if ! pacman -Q attract &>/dev/null && ! pacman -Q attractplus &>/dev/null ; then
    echo "Attract Mode / AttractPlus not installed, groovyarcade won't be installed"
    return
  fi
  echo "Installing emu4crt for Attract Mode"
  for f in efc/*.efc ; do
    /opt/gatools/efc/efc.sh attract "$f"
  done
}


emulationstation_config() {
  if ! pacman -Q emulationstation >/dev/null && ! pacman -Q attractplus ; then
    echo "Emulationstation not installed, groovyarcade won't be installed"
    return
  fi
  echo "Installing emu4crt for EmulationStation"
  for f in efc/*.efc ; do
    /opt/gatools/efc/efc.sh emulationstation "$f"
  done
}
