#!/bin/bash

configure() {
  echo "Configuring GroovyArcade for EmulationStation"
  log_info "Configuring GroovyArcade for EmulationStation"
  install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_FRONTENDS"/emulationstation
  ln -s shared/frontends/emulationstation "$GA_HOME"/.emulationstation
  chown "$GA_USER":"$GA_GROUP" "$GA_HOME"/.emulationstation
  install -m 644 -o "$GA_USER" -g "$GA_GROUP" es_input.cfg "$GA_FRONTENDS"/emulationstation
  install -m 644 -o "$GA_USER" -g "$GA_GROUP" es_systems.cfg "$GA_FRONTENDS"/emulationstation
}