#!/bin/bash

configure() {
  log_info "Configuring GroovyArcade for Final Burn Neo"
  install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_ROMS"/fbneo
  install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_CONFIGS"/fbneo
  install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_CONFIGS"/fbneo/config
  install -o "$GA_USER" -g "$GA_GROUP" -m 644 fbneo.ini "$GA_CONFIGS"/fbneo/config
  ln -s "$GA_CONFIGS"/fbneo "$GA_HOME"/.local/share/fbneo

  attract_config
}


attract_config() {
  if ! pacman -Q attract && ! pacman -Q attractplus >/dev/null ; then
    log_ko "Attract Mode not installed, fbneo won't be installed"
    return
  fi
  echo "Installing fbneo for Attract Mode"
  install -m 644 -o "$GA_USER" -g "$GA_GROUP" "Final Burn Neo.cfg" "$GA_FRONTENDS"/attract/emulators/
}