#!/bin/bash

configure() {
  su $GA_USER <<EOF
mkdir -p "$GA_ROMS"/mame "$GA_CONFIGS"/mame/{artwork,bgfx,cfg,cheat,crosshair,comments,ctrlr,diff,hash,ini,inp,language,nvram,samples,snap,sta,roms,software,dat,fonts}
cp hiscore.ini "$GA_CONFIGS"/mame
EOF
mame_ini_config
ui_ini_config

su $GA_USER <<EOF
[[ ! -e "$GA_HOME"/.mame ]] && ln -s shared/configs/mame "$GA_HOME"/.mame
EOF

attract_config

# Add GM as a frontend
[[ ! -e "$GA_FRONTENDS"/groovymame ]] && ln -s ../configs/mame "$GA_FRONTENDS"/groovymame
chown "$GA_USER":"$GA_GROUP" "$GA_FRONTENDS"/groovymame
}


update() {
  gm_version="$(groovymame -h | grep -oE "v0.[0-9]{3}")"
  # Let's not just overwrite config for versions other thn GM 227
  if [[ $(vercmp "$gm_version" "v.0227") == 0 ]] ; then
    mame_ini_config
  fi
}

mame_ini_config() {
( cd "$GA_CONFIGS"/mame/ ; \
su $GA_USER <<EOF
/usr/lib/mame/groovymame \
    -plugin 'hiscore' \
    -skip_gameinfo '1' \
    -uifont 'uismall.bdf' \
    -video opengl \
    -lowlatency 1 \
    -switchres_ini 1 \
    -modesetting 1 \
    -sound sdl \
    -aspect "4:3" \
    -pluginspath '/usr/lib/mame/plugins' \
    -createconfig
EOF
)
# Had to do it that painful way, $HOME in a su is intepreted as /root
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini homepath '$HOME/shared/configs/mame'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini artpath '$HOME/.mame/artwork;/usr/lib/mame/artwork'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini bgfx_path '/usr/lib/mame/bgfx'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini ctrlrpath '$HOME/.mame/ctrlr;/usr/lib/mame/ctrlr'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini cfg_directory '$HOME/.mame/cfg'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini cheatpath '$HOME/.mame/cheat'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini comment_directory '$HOME/.mame/comments'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini crosshairpath '$HOME/.mame/crosshair'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini diff_directory '$HOME/.mame/diff'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini fontpath '/usr/lib/mame/fonts;$HOME/.mame/fonts;/usr/share/fonts/TTF/'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini hashpath '$HOME/.mame/hash;/usr/lib/mame/hash'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini inipath '$HOME/.mame/ini'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini input_directory '$HOME/.mame/inp'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini languagepath '$HOME/.mame/language;/usr/lib/mame/language'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini nvram_directory '$HOME/.mame/nvram'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini pluginspath '/usr/lib/mame/plugins;$HOME/.mame/plugins'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini samplepath '$HOME/.mame/samples'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini snapshot_directory '$HOME/.mame/snap'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini state_directory '$HOME/.mame/sta'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini rompath '$HOME/shared/roms/mame'
set_mame_config_value "$GA_CONFIGS"/mame/mame.ini swpath '$HOME/.mame/software'
}

ui_ini_config() {
su $GA_USER <<EOF
source /opt/gatools/include/includes.sh
set_mame_config_value ~/shared/configs/mame/ui.ini infos_text_size "1.00"
set_mame_config_value ~/shared/configs/mame/ui.ini font_rows "19"
set_mame_config_value ~/shared/configs/mame/ui.ini historypath "'\$\HOME/.mame/dat\:/usr/share/mame''"
set_mame_config_value ~/shared/configs/mame/plugin.ini hiscore "1"
EOF
}

attract_config() {
  if ! pacman -Q attract &>/dev/null && ! pacman -Q attractplus &>/dev/null ; then
    log_ko "Attract Mode not installed, groovymame won't be installed"
    return
  fi
  log_info "Installing groovymame for Attract Mode"
  sed -riE 's!([[:space:]]*param[[:space:]]+dat_path).*!\1 /usr/share/mame/dat/history.dat!' "$GA_FRONTENDS"/attract/attract.cfg
  #install -m 644 -o "$GA_USER" -g "$GA_GROUP" MAME.cfg "$GA_FRONTENDS"/attract/emulators/
  /opt/gatools/efc/efc.sh attract MAME.efc
}
