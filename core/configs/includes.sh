#!/bin/bash

export GA_SHARED="$GA_HOME"/shared
export GA_ROMS="$GA_SHARED"/roms
export GA_CONFIGS="$GA_SHARED"/configs
export GA_FRONTENDS="$GA_SHARED"/frontends
