#!/bin/bash

source ra.inc.sh

_arch=$(uname -m)
# Can be nightly, or stable/1.9.5
_branch=nightly
_base_url="http://buildbot.libretro.com/${_branch}/linux/${_arch}/latest"
_index_url="${_base_url}/.index-extended"
_download_path=/tmp/ra

# Core info files
_info_url="https://buildbot.libretro.com/assets/frontend/info.zip"

# database file
_database_url="https://buildbot.libretro.com/assets/frontend/database-rdb.zip"
declare -A _systems
declare -A _final_systems_list

_curl_opts="--max-time 10 --silent"

_dirs_list_file=./list_of_dirs

# List cores
# For each core:
#   - download
#   - extract
#   - read the info file
#   - get the required values :


is_a_white_listed_system() {
  for sys in "${!_system_folder[@]}" ; do
    if [[ "$1" == "$sys" ]] ; then
      echo ${_system_folder["$sys"]}
      return 0
    fi
  done
  return 1
}


# List available cores on the libretro buildbot
list_cores() {
  curl ${_curl_opts} "${_index_url}" | cut -d " " -f 3 | sort
}


# list available systems from the libretro databases
list_systems() {
  local target_file="${_download_path}"/database-rdb.zip
  curl ${_curl_opts} "${_database_url}" --output "$target_file"
  [[ $? != 0 || ! -e $target_file ]] && echo "Error downloading from ${_database_url}" && exit 1

  raw_list="$(unzip -Z1 "$target_file" | grep ".rdb$")"
  IFS=$'\n'
  for s in $raw_list ; do
    [[ "${s: -4}" == ".rdb" ]] || continue
    f="$(basename "$s" .rdb)"
    _systems["$f"]=""
  done
  unset IFS
}


# Self explanatory funtion name
download_and_unzip_info_file() {
  local target_file="${_download_path}"/info.zip
  curl ${_curl_opts} --output "$target_file" "${_info_url}"
  [[ $? != 0 || ! -e $target_file ]] && "Error downloading from ${_info_url}"
  unzip -q "$target_file" -d "${_download_path}"
}


# Get a specific field from a libretro .info file
# $1: core name
# $2: expected data
get_info() {
  core_info="$1".info
  key="$2"

  grep "^$key = " "${_download_path}"/"$core_info" | cut -d '"' -f 2
}


# Sorts and dedups a list of extensions
# $1: a list of extensions without the heading . and separated with a |
unique_sort_exts() {
  echo "$1" | tr '|' '\n' | sort -u | paste -sd "|" -
}


# Appends and dedups the valid rom extensions for a system
# $1: a list of systems separated by a |
# $2: a list of extensions without the heading . and separated with a |
update_system_with_exts() {
  local systems="$1"
  local exts="$2"

  IFS=$'\n'
  for s in $(echo $systems | tr '|' '\n') ; do
    # Some system names are not consistent with the database, fix that
    [[ -n ${_system_names_fix["$s"]}  ]] && s=${_system_names_fix["$s"]}
    if [[ -n ${_systems["$s"]} ]] ; then
      _systems["$s"]=${_systems["$s"]}"|$exts"
    else
      _systems["$s"]="$exts"
    fi
    _systems["$s"]="$(unique_sort_exts ${_systems["$s"]})"
  done
  unset IFS
}

# $1: long system name
# $2: short system name
# $3: list of extensions
emulator_file_attract() {
  local exts=".$(echo ${3} | sed -E "s/\|/;./g")"
  [[ ! -d attract ]] && mkdir attract
  cat <<EOF > attract/"$1.cfg"
executable           /opt/galauncher/galauncher.sh
args                 retroarch [system] "[romfilename]"
workdir              \$HOME
rompath              \$HOME/shared/roms/$2/
romext               $exts;.7z;.zip
system               $2
info_source
artwork    flyer           \$HOME/shared/media/$2/flyer
artwork    marquee         \$HOME/shared/media/$2/marquee
artwork    snap            \$HOME/shared/media/$2/snap;\$HOME/shared/media/$2/video
artwork    wheel           \$HOME/shared/media/$2/wheel
EOF
}


# $1: long system name
# $2: short system name
# $3: list of extensions
emulator_file_config() {
  local exts="$(echo ${3} | sed -E "s/\|/ /g")"
  [[ ! -d efc ]] && mkdir efc
  cat <<EOF > efc/"$1.efc"
emulator=retroarch
system=$2
path=\$HOME/shared/roms/$2/
extensions=$exts 7z zip
EOF
}


configure() {
  cores_list=$(list_cores)

  # List cores, consolidate extensions for each system
  for core in $cores_list ; do
    #echo $core
    core_no_ext="$(basename "$core" ".so.zip")"
    local target_file="${_download_path}"/"$core"
    #curl ${_curl_opts} --output "$target_file" "${_base_url}"/"$core"
    #[[ $? != 0 || ! -e $target_file ]] && "Error downloading $core"
    #unzip -q "$target_file" -d "${_download_path}"

    # Get extensions
    exts="$(get_info "$core_no_ext" supported_extensions)"
    db="$(get_info "$core_no_ext" database)"
    [[ -z $db ]] && db="$(get_info "$core_no_ext" systemname)"
    [[ -z $db ]] && db="$(get_info "$core_no_ext" corename)"

    # Get the exact system name
    IFS="|"
    for l in $db ; do
      [[ -n ${_system_names_fix["$l"]} ]] && l=${_system_names_fix["$l"]}
      if dirname="$(is_a_white_listed_system "$l")" ; then
        echo "New valid system ($core_no_ext): $l - $dirname"
        echo "$dirname" >> "$_dirs_list_file"
      fi
    done
    unset IFS

    update_system_with_exts "$db" "$exts"
  done

  # Now prepare the emulator files
  for s in "${!_system_folder[@]}" ; do
    local exts="${_systems["$s"]}"
    echo "$s -> $exts"
    short_name=${_system_folder["$s"]}
    #emulator_file_attract "$s" "$short_name" "$exts"
    emulator_file_config "$s" "$short_name" "$exts"
  done
}


# Create the temporary download folder
[[ ! -d "${_download_path}" ]] && mkdir -p "${_download_path}"
[[ -e $_dirs_list_file ]] && rm "$_dirs_list_file"

list_systems
download_and_unzip_info_file
configure

sort -u "$_dirs_list_file" -o "$_dirs_list_file"
sed -i "/^$/d" "$_dirs_list_file"

rm -rf "${_download_path}"




for key in "${!_systems[@]}"; do echo -e "$key\t"${_systems["$key"]}; done | sort | column -ts $'\t'
