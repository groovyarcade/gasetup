#!/bin/bash


configure() {
log_info "Configuring GroovyArcade for Retroarch"
install -o "$GA_USER" -g "$GA_GROUP" -d "$MOTHER_OF_ALL"/{bios,saves/retroarch}
install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_CONFIGS"/retroarch
while read line ; do
  install -o "$GA_USER" -g "$GA_GROUP" -d "$GA_ROMS"/"${line%/*}"
done < /opt/gasetup/core/configs/retroarch/list_of_dirs
install -o "$GA_USER" -g "$GA_GROUP" /etc/retroarch.cfg "$GA_CONFIGS"/retroarch/retroarch.cfg
# Check for an already existing installation
if [[ -d "$GA_HOME"/.config/retroarch ]] ; then
  log_warn "Found an existing retroarch user folder. Renaming it"
  mv "$GA_HOME"/.config/retroarch "$GA_HOME"/.config/retroarch.before_gasetup
fi
ln -s "$GA_CONFIGS"/retroarch "$GA_HOME"/.config/retroarch
ln -s "$MOTHER_OF_ALL"/"bios" "$GA_HOME"/.config/retroarch/system
chown -R "$GA_USER":"$GA_GROUP" "$GA_HOME"/.config/retroarch

retroarch_cfg_config
# Disabled as it installs all systems despite the presence or not of cores
#attract_config

# Add RA as a frontend
[[ ! -e "$GA_FRONTENDS"/retroarch ]] && ln -s ../configs/retroarch "$GA_FRONTENDS"/retroarch
chown "$GA_USER":"$GA_GROUP" "$GA_FRONTENDS"/retroarch
}


retroarch_cfg_config() {
set_ra_config_value input_joypad_driver udev
set_ra_config_value menu_driver rgui
set_ra_config_value audio_driver alsa
set_ra_config_value video_fullscreen true
set_ra_config_value crt_switch_resolution 4
set_ra_config_value crt_switch_resolution_super 0
set_ra_config_value menu_show_advanced_settings true
set_ra_config_value quit_press_twice false
set_ra_config_value input_menu_toggle tab
set_ra_config_value system_directory "$MOTHER_OF_ALL/bios/"
set_ra_config_value savefile_directory "$MOTHER_OF_ALL/saves/retroarch/"
set_ra_config_value savestate_directory "$MOTHER_OF_ALL/saves/retroarch/"
set_ra_config_value rgui_browser_directory "$MOTHER_OF_ALL/roms"
set_ra_config_value rgui_config_directory "$MOTHER_OF_ALL/configs/retroarch/config"
set_ra_config_value video_driver "glcore"
set_ra_config_value video_max_swapchain_images "2"
set_ra_config_value menu_linear_filter "true"
set_ra_config_value menu_rgui_shadows "true"
set_ra_config_value menu_widget_scale_auto "false"
set_ra_config_value menu_widget_scale_factor "2.500000"

set_config_value "$GA_CONF" retroarch.mastersystem.core genesis_plus_gx
set_config_value "$GA_CONF" retroarch.megadrive.core genesis_plus_gx
set_config_value "$GA_CONF" retroarch.nes.core nestopia
set_config_value "$GA_CONF" retroarch.n64.core mupen64plus_next
set_config_value "$GA_CONF" retroarch.pcengine.core mednafen_pce
set_config_value "$GA_CONF" retroarch.segacd.core genesis_plus_gx
set_config_value "$GA_CONF" retroarch.saturn.core kronos
set_config_value "$GA_CONF" retroarch.fds.core mesen
set_config_value "$GA_CONF" retroarch.gb.core gambatte
set_config_value "$GA_CONF" retroarch.gbc.core gambatte
set_config_value "$GA_CONF" retroarch.nds.core desmume
set_config_value "$GA_CONF" retroarch.dc.core flycast
}

attract_config() {
  if ! pacman -Q attract && ! pacman -Q attractplus >/dev/null ; then
    log_ko "Attract Mode not installed, retroarch won't be installed"
    return
  fi
  log_info "Installing Retroarch systems for Attract Mode"
  #install -m 644 -o "$GA_USER" -g "$GA_GROUP" attract/*.cfg "$GA_FRONTENDS"/attract/emulators/
  for f in efc/*.efc ; do
    /opt/gatools/efc/efc.sh attract "$f"
  done
}
