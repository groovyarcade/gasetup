#!/bin/bash
# Needs $GA_HOME
source /opt/gatools/include/includes.sh

bootloaders_LEGACY=("SYSLINUX" "GRUB" "LIMINE")
bootloaders_UEFI=("SYSLINUX" "EFISTUB" "GRUB" "SYSTEMDBOOT" "LIMINE")
BOOTPARAMS_CONF="$GA_CONF_DIR"/boot-params.conf
default_cmdline="rd.udev.log-priority=3 splash mitigations=off audit=0 consoleblank=0"
bootparams_flag=/tmp/gasetup.bootparams

detect_bootmode() {
    if [[ -d /sys/firmware/efi/efivars ]]
    then
        echo "UEFI"
    else
        echo "LEGACY"
    fi
}

get_boot_device() {
    lsblk -lpno pkname $(get_boot_partition)
}

get_boot_partition() {
    if booted_on_iso ; then
      findmnt -kno SOURCE /groovyarcade/boot
    else
      findmnt -kno SOURCE /boot
    fi
}

get_root_partition() {
    if booted_on_iso ; then
      findmnt -kno SOURCE /groovyarcade
    else
      findmnt -kno SOURCE /
    fi
}

get_root_PARTUUID() {
    blkid -s PARTUUID -o value $(get_root_partition)
}

gen_root_kernel_param() {
    echo "root=PARTUUID=\"$(get_root_PARTUUID)\""
}

detect_bootloader_UEFI()
{
    # the ESP is FAT32, so convert \ to / for easier parsing
    boot_partuuid=$(get_root_PARTUUID)
    # We're only considering the current installation, not multiple installs on a same PC
    # This is "locked" by searching on the boot partition PARTUUID
    efi_bootloader="$(efibootmgr | grep -oE "$boot_partuuid,.*" | sed -E 's+.*/(.*)+\1+' | tr '\\' '/')"
    found_bootloaders=()

    # Not tested yet
    for efibl in $efi_bootloader
    do
      for bl in ${bootloaders_UEFI[@]}
      do
          if detect_${bl}_UEFI "$efibl"
          then
              found_bootloaders+=("$bl")
          fi
      done
    done
    echo ${found_bootloaders[*]}
}

detect_bootloader_LEGACY() {
    for bl in ${bootloaders_LEGACY[@]}
    do
        if detect_${bl}_LEGACY
        then
            echo "$bl"
            break
        fi
    done
}

install_boot_loader() {
    local bootloader=LIMINE
    [[ -n "$1" ]] && bootloader="$1"
    install_${bootloader}_"$(detect_bootmode)"
}

configure_boot_loader() {
    if [[ -n "$1" ]] ; then
        local bootloaders="$1"
    else
        local bootloaders="$(detect_bootloader_"$(detect_bootmode)")"
    fi
    # if there is a space in a boot loader name, we're screwed here
    # As well as with hybrid MBR/GPT booting since we just do UEFI or LEGACY
    for b in $bootloaders ; do
        [[ $(type -t configure_"$b") == function ]] || notify "Couldn't find $b"
        configure_"$b"
    done
}

full_update_and_install() {
    for p in "$@" ; do
      pacman -Q "$p" &>/dev/null || break
    done
    ask_yesno "Installing $p requires a full update. Proceed ?" || return 1
    pacman -Syu --noconfirm --needed archlinux-keyring
    pacman -Sy --noconfirm --needed "$@"
    return $?
}

# This helps to install rolling or LTS kernel
get_initramfs_name() {
    local k=$(get_config_value "$GA_CONF" kernel)
    k="${k:-linux-15khz}"
    echo "initramfs-${k}.img"
}

get_kernel_name() {
    local k=$(get_config_value "$GA_CONF" kernel)
    k="${k:-linux-15khz}"
    echo "vmlinuz-${k}"
}

# Check if the booted kernel is the one set in ga.conf, and warn eventually
check_kernel_selection() {
    local k="$(grep -oE '\([a-z0-9@-]+\)' /proc/version | sed -E 's/\((.*)@.*\)/\1/')"
    local set_k=$(get_config_value "$GA_CONF" kernel)
    set_k="${set_k:-linux-15khz}"
    if [[ "$k" != "$set_k" ]] ; then
        notify "The running kernel differs from the one set in ga.conf. The parameters will be set according to ga.conf. You can select a different kernel in gasetup"
    fi
}
### BOOTLOADERS SPECIFIC MANAGEMENT
# Bootloaders have 5 specific functions, each declined for BIOS and UEFI (except configure):
#   - detect -> return 0 if the the bootloader is the current one
#   - configure -> write the required parameters to the bootloader configuration file (SYSLINUX, GRUB, SYSTEMDBOOT)
#   - install_..._{LEGACY,UEFI} -> install the bootloader
#   - update -> apply the new configuration (EFISTUB, GRUB)
#   - install_package -> install a package if required
#   - intall -> encapsulate install_package and bootloader installation


### SYSLINUX ###
detect_SYSLINUX_LEGACY() {
    # Well, the day the syslinux mdr.bin is updated but not the MBR, this won't work
    diff <(dd if="$(get_boot_device)" bs=440 count=1 2>&1 status=none) <(cat /usr/lib/syslinux/bios/mbr.bin) &>/dev/null
}

detect_SYSLINUX_UEFI() {
    local efi_boot_string="$1"
    if [[ "$efi_boot_string" == '/EFI/syslinux/syslinux.efi' ]]
    then
        return 0
    elif grep -qoi 'syslinux' /boot/"$efi_boot_string"
    then
        return 0
    elif strings /boot/"$efi_boot_string" | grep -i syslinux
    then
        return 0
    fi
    return 1
}

configure_SYSLINUX() {
    check_kernel_selection
    local syslinux_cfg=/boot/syslinux/syslinux.cfg
    local boot_mode=$(detect_bootmode)
    [[ $boot_mode == "UEFI" ]] && syslinux_cfg=/boot/EFI/syslinux/syslinux.cfg
    booted_on_iso && syslinux_cfg=/groovyarcade/"$syslinux_cfg"
    # Need th / UUID disk + 1 liner kernel parameters
    cat <<EOF > "$syslinux_cfg"
default arch
timeout 0
prompt 0
#UI vesamenu.c32
menu title Groovy Arcade Linux
menu background splash.png
label arch
menu label GroovyArcade
linux /$(get_kernel_name)
append $(get_root_param) $(get_kernel_boot_params)
initrd /$(get_initramfs_name)
EOF
    return
}

install_SYSLINUX() {
    if booted_on_iso ; then
        pacstrap -P -K /groovyarcade syslinux
    else
        install_package_SYSLINUX
    fi
    ${FUNCNAME}_"$(detect_bootmode)"
}

install_SYSLINUX_LEGACY() {
    sudo syslinux-install_update -i -a -m
}

install_SYSLINUX_UEFI() {
    delete_current_gauefi
    sudo efibootmgr --create --disk "$(get_boot_device)" --part 1 --loader /EFI/syslinux/syslinux.efi --label "GAUEFI" --verbose
}

# SYSLINUX doesn't need any command to update its boot
update_SYSLINUX_LEGACY() {
    return
}

update_SYSLINUX_UEFI() {
    return
}

install_package_SYSLINUX() {
    full_update_and_install syslinux || return 1
}

### EFISTUB ###
detect_EFISTUB_UEFI() {
    local efi_boot_string="$1"
    # Not working with an alternate kernel (lts, zen, etc ...)
    if [[ "$efi_boot_string" == '/vmlinuz-linux-15khz' ]]
    then
        return 0
    fi
    return 1
}

configure_EFISTUB() {
    # UselessNo configuration file since it's all in the UEFI NVRAM, but that is done with install
    check_kernel_selection
    install_EFISTUB_UEFI
    return
}

install_EFISTUB_UEFI() {
    # Here we'll need the partition GUID + full kernel parameters'
    delete_current_gauefi
    sudo efibootmgr --create --disk "$(get_boot_device)" --part 1 --label "GAUEFI" --loader "/$(get_kernel_name)" --unicode " $(get_root_param) $(get_kernel_boot_params) initrd=\\$(get_initramfs_name)" --verbose
}

update_EFISTUB_UEFI() {
    # Need to delete and reinstall the EFI stub
    delete_current_gauefi
    install_EFISTUB_UEFI
    return
}


### GRUB ###
detect_GRUB_LEGACY() {
    sudo dd bs=512 count=1 if="$(get_boot_device)" 2>/dev/null | strings | tr -d ' ' | grep -oq "^GRUB$"
}

detect_GRUB_UEFI() {
    local efi_boot_string="$1"
    if [[ "$efi_boot_string" == '\EFI\GRUB\grubx64.efi' ]]
    then
        return 0
    elif grep -qoi 'grub' /boot/"$efi_boot_string"
    then
        return 0
    elif strings /boot/"$efi_boot_string" | grep -i grub
    then
        return 0
    fi
    return 1
}

configure_GRUB() {
    # Everything that must be edited is in /etc/default/grub
    # kernel parameters go to GRUB_CMDLINE_LINUX_DEFAULT
    # Set GRUB_DISTRIBUTOR="GroovyArcade"
    # Set GRUB_TIMEOUT=0
    # sed -E 's+message=\"\$\(gettext_printf \"Loading .*+message=+' /etc/grub.d/10_linux -> remove grub "loading blablabla"
    local grub_conf=/etc/default/grub
    booted_on_iso && grub_conf=/groovyarcade"$grub_conf"
    check_kernel_selection
    #~ sudo sed -i -e 's/GRUB_CMDLINE_LINUX_DEFAULT=.*/GRUB_CMDLINE_LINUX_DEFAULT="'"$(get_kernel_boot_params)"'"/' \
      #~ -e 's/GRUB_DISTRIBUTOR=.*/GRUB_DISTRIBUTOR="GroovyArcade"/' \
      #~ -e 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' \
      #~ "$grub_conf"
    # Only root can do it, but consider we are already root here
    set_config_value "$grub_conf" GRUB_CMDLINE_LINUX_DEFAULT  "\"$(get_kernel_boot_params)\""
    set_config_value "$grub_conf" GRUB_DISTRIBUTOR "GroovyArcade"
    set_config_value "$grub_conf" GRUB_TIMEOUT "0"
    set_config_value "$grub_conf" GRUB_DISABLE_LINUX_UUID "true"
    set_config_value "$grub_conf" GRUB_DISABLE_LINUX_PARTUUID "false"
    set_config_value "$grub_conf" GRUB_DISABLE_OS_PROBER "true"
    update_GRUB_"$(detect_bootmode)"
    return
}

install_GRUB() {
    if booted_on_iso ; then
        pacstrap -P -K /groovyarcade grub
    else
        install_package_GRUB
    fi
    ${FUNCNAME}_"$(detect_bootmode)"
}

install_GRUB_LEGACY() {
    local boot_dir=/boot
    if booted_on_iso ; then
        grub_conf=/groovyarcade"$grub_conf"
        pacstrap -P -K /groovyarcade grub
        sudo arch-chroot /groovyarcade grub-install --target=i386-pc --boot-directory="/boot" "$(get_boot_device)"
    else 
        grub-install --target=i386-pc --boot-directory="/boot" "$(get_boot_device)"
    fi
    update_GRUB_LEGACY
}

install_GRUB_UEFI() {
    delete_current_gauefi
    # This sucks: grub will create the /boot/EFI/<folder> with the name of the --bootloader-id value
    if booted_on_iso ; then
        sudo arch-chroot /groovyarcade  grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="GAUEFI"
    else
        sudo grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="GAUEFI"
    fi
    update_GRUB_UEFI
}

update_GRUB_LEGACY() {
    local boot_dir=/boot
    if booted_on_iso ; then
        sudo arch-chroot /groovyarcade grub-mkconfig -o /boot/grub/grub.cfg
    else
        sudo grub-mkconfig -o /boot/grub/grub.cfg
    fi
}

update_GRUB_UEFI() {
    update_GRUB_LEGACY
}

install_package_GRUB() {
    full_update_and_install grub || return 1
}

### SYSTEMD-BOOT ###
detect_SYSTEMDBOOT_UEFI() {
    local efi_boot_string="$1"
    if [[ "$efi_boot_string" == '/EFI/systemd/systemd-bootx64.efi' ]]
    then
        return 0
    fi
    return 1
}

configure_SYSTEMDBOOT() {
    local systemdboot_conf=/boot/loader/loader.conf
    booted_on_iso && systemdboot_conf=/groovyarcade/"$systemdboot_conf"
    check_kernel_selection
    cat > "systemdboot_conf" <<EOF
timeout 0
default ga.conf
EOF
    cat > /boot/loader/entries/ga.conf <<EOF
title   GroovyArcade
linux   /$(get_kernel_name)
initrd  /intel-ucode.img
initrd  /amd-ucode.img
initrd  /$(get_initramfs_name)
options $(get_root_param) $(get_kernel_boot_params)
EOF
    return
}

install_SYSTEMDBOOT_UEFI() {
    delete_current_gauefi
    bootctl install
}

update_SYSTEMDBOOT_UEFI() {
    return
}

install_package_SYSTEMDBOOT() {
    full_update_and_install systemd-boot-pacman-hook
}

### LIMINE ###
detect_LIMINE_LEGACY() {
    dd if="$(get_boot_device)" bs=440 count=1 2>&1 status=none | grep -qi LIMINE
}

detect_LIMINE_UEFI() {
  # Get the EFI 
  local efi_boot_string="$1"
  [[ "$efi_boot_string" == "/EFI/BOOT/BOOTX64.EFI" ]] || return 1
  strings "/boot$efi_boot_string" | grep -qi limine
}

configure_LIMINE() {
    local limine_conf=/boot/limine.conf
    booted_on_iso && limine_conf=/groovyarcade"$limine_conf"
    check_kernel_selection
    mkdir -p /boot/limine
    cat > "$limine_conf" <<EOF
timeout: 0
quiet: yes

/GroovyArcade
    protocol: linux
    kernel_path: boot():/$(get_kernel_name)
    kernel_cmdline: $(get_root_param) $(get_kernel_boot_params)
    module_path: boot():/$(get_initramfs_name)

EOF
    return
}

install_LIMINE() {
    if booted_on_iso ; then
        pacstrap -P -K /groovyarcade limine liminedeploy-hook
    else
        install_package_LIMINE
    fi
    ${FUNCNAME}_"$(detect_bootmode)"
}

install_LIMINE_LEGACY() {
    # When booting on ISO, consider limine is already installed on it, unlike GRUB
    local bootdest=/boot
    booted_on_iso && bootdest=/groovyarcade"$bootdest"
    cp /usr/share/limine/limine-bios.sys "$bootdest"
    limine bios-install "$(get_boot_device)"
    return
}

install_LIMINE_UEFI() {
    delete_current_gauefi
    local bootdest=/boot
    booted_on_iso && bootdest=/groovyarcade"$bootdest"
    install -D /usr/share/limine/BOOTX64.EFI "$bootdest"/EFI/BOOT/BOOTX64.EFI
    sudo efibootmgr --create --disk "$(get_boot_device)" --part 1 --loader /EFI/BOOT/BOOTX64.EFI --label "GAUEFI" --verbose
    return
}

install_package_LIMINE() {
    full_update_and_install limine liminedeploy-hook || return 1
}

#
#
#

get_kernel_boot_params() {
    # check if we're on ISO
    if booted_on_iso && [[ -n $default_cmdline ]] ; then
      echo "rw $default_cmdline $kernel_video_params"
    elif [[ ! -f "$BOOTPARAMS_CONF" ]] ; then
        tr ' ' '\n' < /proc/cmdline | grep -v '^$' | grep -vE "^(BOOT_IMAGE|root|initrd)=" > "$BOOTPARAMS_CONF"
        chown $_USER:$_USER "$BOOTPARAMS_CONF"
        cat "$BOOTPARAMS_CONF" | tr '\n' ' '
    else
        cat "$BOOTPARAMS_CONF" | tr '\n' ' '
    fi
}

get_root_param() {
    echo "root=PARTUUID=$(get_root_PARTUUID)"
}

delete_current_gauefi() {
    boot_num=$(efibootmgr | grep -E "^Boot[0-9]{4}. GAUEFI" | grep -oE "[0-9]{4}" | head -1)
    [[ ! -z $boot_num ]] && sudo efibootmgr -B -b "$boot_num"
    [[ -z $boot_num ]] && boot_num=$(efibootmgr | grep -E "^Boot[0-9]{4}. Linux Boot Manager" | grep -oE "[0-9]{4}" | head -1) && [[ ! -z $boot_num ]] && sudo efibootmgr -B -b "$boot_num"
}


### Now the UI part
# Make a menu entry per kernel boot parameter to edit it
split_commandline_to_conf() {
    # https://elixir.bootlin.com/linux/v6.13-rc3/source/lib/cmdline.c#L222-L226
    # So the following technique will break if some spaces are between quotes
    # Get rid of root= and rw parameters, will add them when configuring
    if [[ ! -e "$bootparams_flag" ]] ; then
        cat /proc/cmdline | tr ' ' '\n' | grep -vE '(root=|rw)' > "$BOOTPARAMS_CONF"
        chown $_USER:$_USER "$BOOTPARAMS_CONF"
        touch "$bootparams_flag"
    fi
}
# Select the boot loader, highlight the current one
worker_edit_kernel_parameters()
{
    # If the bootparams.conf doesn't exist, time to create it
    # check that bootparams.conf reflects current /proc/cmdline, eventually warn
    # This is not done yet, a bit rough, shouldn't do this once out of edit
    set -x
    split_commandline_to_conf
    
    default=1
    while true ; do
        add_param="$(($(wc -l < "$BOOTPARAMS_CONF" ) + 1))"
        apply_settings="$(($(wc -l < "$BOOTPARAMS_CONF" ) + 2))"
        line_selected=
        ask_option $default "Kernel arguments" "Select a parameter to edit" \
            'required' \
            $(cat --number "$BOOTPARAMS_CONF" | tr '\n' ' ' | tr -s ' ' | tr '\t' ' ') \
            $add_param "Add a new parameter" \
            $apply_settings "Apply settings (needs reboot)"
        line_selected="$ANSWER_OPTION"
        [[ -z "$line_selected" ]] && return
        if [[ "$line_selected" == $add_param ]] ; then
            default=1
            value="$(execute worker edit_kernel_parameter "")"
            [[ "$?" == 0 ]] && echo "$value" >> "$BOOTPARAMS_CONF"
            continue
        # Apply new settings to the bootloader config
        elif [[ "$line_selected" == $apply_settings ]] ; then
            ask_yesno "The configuration files will be overwritten. Proceed ?" yes || return
            configure_boot_loader
            ask_yesno "Reboot now ?" && reboot
            return
        fi
        default="$(($line_selected + 1))"
        local param="$(head -"$line_selected" "$BOOTPARAMS_CONF" | tail -1)"
        execute worker edit_kernel_parameter "$param"
        if [[ "$?" == 1 ]] ; then
            rm bootparams_flag
            split_commandline_to_conf
            continue
        fi
        # Update the file with the new parameters
        sed -i \
            -e "${line_selected}s+.*+$ANSWER_STRING+" \
            -e "/^$/d" "$BOOTPARAMS_CONF" \
            "$BOOTPARAMS_CONF"
    done
    set +x
    return
}

worker_edit_kernel_parameter()
{
    parameter="$1"
    ask_string "Set value" "$parameter" || return 1
    echo "$ANSWER_STRING"
}

worker_select_bootloader() {
    # List the possible bootloaders
    local boot_mode="$(detect_bootmode)"
    if [[ $boot_mode == "LEGACY" ]] ; then
        local bootloaders_list=$(for i in $(seq 1 ${#bootloaders_LEGACY[@]}) ; do echo -n "$i ${bootloaders_LEGACY[$((i - 1))]} " ; done)
    elif [[ $boot_mode == "UEFI" ]] ; then
        local bootloaders_list=$(for i in $(seq 1 ${#bootloaders_UEFI[@]}) ; do echo -n "$i ${bootloaders_UEFI[$((i - 1))]} " ; done)
    else
        notify "Couldn't detect boot mode"
        return 1
    fi
    # Get the current bootloader
    default=1
    
    ask_option "no" "Bootloader" "Select a new boootloader" 'required' $bootloaders_list || return 1
    # The line under works from a shell but not in the script. Couldn't find why
    #local b="$(eval echo \${bootloaders_${boot_mode}[$(($ANSWER_OPTION - 1))]})"
     if [[ $boot_mode == "LEGACY" ]] ; then
        b=${bootloaders_LEGACY[$(($ANSWER_OPTION - 1))]}
    elif [[ $boot_mode == "UEFI" ]] ; then
        b=${bootloaders_UEFI[$(($ANSWER_OPTION - 1))]}
    fi

    # Inform about the risk
    ask_yesno "Changing bootloader can break booting. Proceed ?" || return 1
    if ! [[ $(type -t configure_"$b") == function ]] ; then
        notify "Missing configure function for $b"
        return
    fi
    if ! [[ $(type -t install_"$b"_"$boot_mode") == function ]] ; then
        notify "Missing install function for $b"
        return
    fi
    # Install the required package
    if [[ "GRUB LIMINE SYSLINUX SYSTEMDBOOT" == *"$b"* ]]; then
        install_package_${b}
    fi
    configure_$b
    install_"$b"_"$boot_mode"
}
# This is some testing
#set -x
#boot_mode=$(detect_bootmode)
#detect_bootloader_${boot_mode}
#install_SYSTEMDBOOT_UEFI
#configure_SYSTEMDBOOT
#configure_GRUB
#install_GRUB_${boot_mode}
#install_SYSLINUX_${boot_mode}
#configure_SYSLINUX
#[[ $boot_mode == "UEFI" ]] && install_EFISTUB_${boot_mode}
