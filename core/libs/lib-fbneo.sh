#!/bin/bash

worker_setup_fbneo() {
  # Check if fbneo is installed, install it if required
  if pacman -Qi "fbneo" &>/dev/null ; then
    inform "FBNeo is already installed" && sleep 2
    return 0
  fi

  ask_yesno "Install fbneo?" ||return 1
  ask_yesno "You must first make a full system update first. Do it?" || return 1
  execute worker system_full_update
  if ! sudo pacman -S fbneo ; then
    inform "Couldn't install Final Burn Neo!" && sleep 2
    return 2
  fi
  /opt/gasetup/gasetup.sh -p interactive -c fbneo -u "$GA_USER"
  inform "Final Burn Neo is now installed!" && sleep 2
  return 0
}