#!/bin/bash
ESP_PATH=/boot
WITH_SWAP=no

# Returns the needed swap size in GB
# Based on https://anto.online/code/bash-script-to-create-a-swap-file-in-linux/
decide_swap_size() {
  #get available physical ram
  local availMemMb=$(grep MemTotal /proc/meminfo | awk '{print $2}')

  #convert from kb to mb to gb
  local gb=$(awk "BEGIN {print $availMemMb/1024/1204}")

  #round the number to nearest gb
  local gb=$(echo $gb | awk '{print ($0-int($0)<0.499)?int($0):int($0)+1}')

  if [[ $gb == 0 ]] ; then
    notify "Couldn't detect the memory size, aborting swap file creation"
    lok_ko "Failed getting RAM size for swap size"
    return 1;
  fi

  # This calculation is not taking into account hibernation ...
  if [[ $gb -le 2 ]];  then
    swapSizeGb=2
  elif [[ $gb -gt 2 && $gb -lt 32 ]] ; then
    let swapSizeGb=4+$gb-2
  elif [[ $gb -gt 32 ]] ; then
    let swapSizeGb=$gb
  fi
  echo "$swapSizeGb"
  return 0
}

# Partition a disk for UEFI booting
# $1: the device (ex: /dev/sda)
uefi_partition_disk() {
  local disk="$1"
  # Some blockdevices need a p before a partition number
  # https://unix.stackexchange.com/a/500910 + next answer

  sgdisk $disk -o > /dev/null
  sgdisk $disk -n 1::+512MiB -t 1:C12A7328-F81F-11D2-BA4B-00A0C93EC93B  || return 1
  sgdisk $disk -n 2 -t 2:0FC63DAF-8483-4772-8E79-3D69D8477DE4  || return 1
  return 0
}

bios_partition_disk() {
  local disk="$1"
  local swapsize=

  BOOT=400

  ## FDISK AUTO PARTITION START
  echo "
o
n
p
${BOOTNUM}

+${BOOT}M
t
0b

n
p
${ROOTNUM}


a
1
w
q
" | fdisk "$disk"
  return $?
}


# This will help setting the device name with partitions
# ex: mmcblk0, nvme0n1 -> ned a p, which is not the case with sda
disk_device_partition() {
  local base_dev="$(basename $1)"
  local part_sep=""

  # Devices like nvme or eMMC need a p before the partition number
  # That's because the last char of such device is a digit
  # ex: mmcblk0, nvme0n1
  if [[ "$base_dev" =~ mmcblk[0-9]+ || \
      ! -e "/sys/class/block/${base_dev}/device/type" || \
      $(cat "/sys/class/block/${base_dev}/device/type") != 0 \
    ]]; then
    part_sep="p"
  fi
  echo "$1"$part_sep
}


# Auto partition a disk device, UEFI or BIOS
# $1: the disk device (ex: /dev/sda)
autopartition_disk() {
  local disk="$1"
  local disk_p="$(disk_device_partition "$disk")"

  # Make sure the disk device is valid
  lsblk "$disk" &> /dev/null || return 1
  log_info "Preparing device $disk"
  wipefs --all --force "$disk" || return 1

  # Determine if we go GPT or MBR
  if [[ -d /sys/firmware/efi/efivars ]] ; then
    log_info "This system was booted on UEFI, going the GPT way"
    uefi_partition_disk "$disk"
  else
    log_info "This system was booted on BIOS, going the MBR way"
    bios_partition_disk "$disk"
  fi
  if [[ $? != 0 ]]; then
    notify "Partitioning Error!!!"
    return 1
  fi
  inform "Partitioning Successful"
  partprobe "$disk"

  # Format parrtitions
  local bootpart="${disk_p}"1
  local mainpart="${disk_p}"2

  mkfs.vfat -F32 $bootpart
  fatlabel "${bootpart}" GABOOT
  mkfs.ext4 -F -O ^64bit -q $mainpart
  e2label "${mainpart}" GA
  return 0
}


auto_partition_and_mount_disk() {
  local disk="$1"
  local disk_p="$(disk_device_partition "$disk")"
  local swapsize=

  autopartition_disk "$disk" || return 1

  # This will turn to hell if there is already a GA install
  boot_dev="${disk_p}"1
  root_dev="${disk_p}"2

  [[ ! -d /groovyarcade ]] && mkdir /groovyarcade
  mount ${root_dev} /groovyarcade/

  [[ ! -d /groovyarcade/boot ]] &&  mkdir -p /groovyarcade/boot
  mount ${boot_dev} /groovyarcade/boot

  if [[ $WITH_SWAP == 'yes' ]] ; then
    if ! swapsize="$(decide_swap_size)" ; then
      log_warn "Failed to get swap size, force it to 4G"
      swapsize=4
    fi
    # This doesn't work with all FS, but since GA is ext4, it's fine
    # see https://wiki.archlinux.org/title/Swap#Swap_file
    mkswap -U 0657FD6D-A4AB-43C4-84E5-0933C84B4F4F --size ${swapsize}G --file /groovyarcade/.swapfile
    swapon /groovyarcade/.swapfile
  fi
}


worker_select_and_partition_disk() {
  local default=no
  lsblk -o MOUNTPOINT | grep -q "^/media" && sudo umount /media/* >/dev/tty12 2>&1
  local DEVICE=
  DISKS=$(for dev in $(lsblk -nde 2,7,11 -o NAME); do
    size=$(lsblk --output SIZE -n -d "/dev/$dev" | tr -d ' ')
    name=$(hwinfo --disk --only /dev/$dev | grep "Model:" | cut -d '"' -f 2 | tr " " "_")
    echo "/dev/$dev ($name/$size)"
  done)
  if [[ -z "$DISKS" ]]; then
    notify "No drives available to partition"
    return 1
  fi

  ask_option $default "Automatically Partition" "Choose a drive to auto-partition" required $DISKS \
  "Done" "-" || return 1
  echo $?

  [[ "$ANSWER_OPTION" == "Done" ]] && return 1

  DOIT=yes
  ask_yesno "Really auto-partition $ANSWER_OPTION drive?\nWarning! All data will be erased!!!" || DOIT=no
  [[ ! "$DOIT" == "yes" ]] && return 1
  DEVICE="$ANSWER_OPTION"
  ask_yesno "Do you want to skip swap?" || WITH_SWAP=yes
  inform "Autopartitioning $DEVICE..."
  log_info "Installing to $DEVICE"
  auto_partition_and_mount_disk "$DEVICE"
  INSTALL_DRIVE="$DEVICE"
  return $?
}
