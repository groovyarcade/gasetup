#!/bin/bash

LIB_LOCALIZATION=1


guess_timezone() {
  tz="$(curl -s --fail https://ipapi.co/timezone)" || return 1
  echo $tz
  return 0
}


guess_x11_keyboard() {
  country="${1,,}"
  localectl list-x11-keymap-layouts | grep -qE "^${country}$" || return 1
  echo $country
  return 0
}


guess_keyboard() {
  country="${1,,}"

  # Try various possibilities
  if localectl list-keymaps | grep -qE "^${country}$" ; then
    keymap=$(localectl list-keymaps | grep -E "^${country}$")
  elif localectl list-keymaps | grep -qE "^${country}-latin9$" ; then
    keymap=$(localectl list-keymaps | grep -E "^${country}-latin9$")
  elif localectl list-keymaps | grep -qE "^${country}-latin1$" ; then
    keymap=$(localectl list-keymaps | grep -E "^${country}-latin1$")
  else
    return 1
  fi

  echo $keymap
  return 0
}


guess_locale() {
  local country="${1,,}"
  local final_locale=

  if grep -qE "^#?${country}_${country^^}\.UTF-8" /etc/locale.gen ; then
    final_locale="${country}_${country^^}.UTF-8"
    sed -Ei "s/^#(${country}_${country^^}\.UTF-8.*)/\1/" /etc/locale.gen
  elif grep -qE "^#?${country}_${country^^} " /etc/locale.gen ; then
    final_locale="${country}_${country^^}"
    sed -Ei "s/^#(${country,,}_${country^^} .*)/\1/" /etc/locale.gen
  elif grep -qE "^#?en_${country^^}\.UTF-8" /etc/locale.gen ; then
    final_locale="en_${country^^}.UTF-8"
    sed -Ei "s/^#(en_${country^^} .*)/\1/" /etc/locale.gen
  elif grep -qE "^#?en_${country^^}\.UTF-8.*" /etc/locale.gen ; then
    final_locale="${country}_${country^^}"
    sed -Ei "s/^#(${country,,}_${country^^} .*)/\1/" /etc/locale.gen
  else
    # We couldn't find a suitable locale, or the locale has already been set
    return 1
  fi

  echo $final_locale
  return 0
}


set_locales() {
  local tz="$1"
  local lang="$2"
  local kbd="$3"
  local x11kbd="$4"

  log_info "Set timezone to: $tz"
  sudo timedatectl set-timezone "$tz"


  log_info "Set locale to: $lang"
  sed -Ei "s/^#($lang)/\1/" /etc/locale.gen
  echo "LANG=$lang" > /etc/locale.conf
  locale-gen -a >&2
  localectl set-locale LANG="$lang"

  log_info "Set keyboard to: $kbd"
  [[ ! -f etc/vconsole.conf ]] && sudo touch /etc/vconsole.conf
  localectl --no-convert set-keymap "$kbd"
  localectl set-x11-keymap "$x11kbd" pc105
  sudo loadkeys "$kbd"
}


auto_configure_localization() {
  # Do we have internet ?
  if ! ping -c1 -W1 www.google.com >/dev/null 2>&1; then
    inform "You need an internet connection for locale autodetection" ; sleep 2
    return 1
  fi

  # Internet is here, let's start the party
  local tz=$(guess_timezone)

  # Get the country, will be useful for next steps
  local country=$(curl -s ipinfo.io | grep country | cut -d '"' -f 4)

  # Guess the keyboard according to the country
  local kb=$(guess_keyboard "$country")

  # find the possible X11 keyboard
  local x11kb=$(guess_x11_keyboard "$country")

  # Now go for the locale
  local the_locale=$(guess_locale "$country")

  # Ask for user agreement before setting all
  info_msg=$(echo -e "Timezone will be set to: $tz\nKeyboard will be set to: $kb\nX11 keboard will be set to: $x11kb\nLocale will be set to: $the_locale\nApply these new settings ?")
  ! ask_yesno "$info_msg" && return 2

  set_locales "$tz" "$the_locale" "$kb" "$x11kb"

  ask_yesno "Do you want to enable NTP?" || return 0
  # We're using gatools code, can't be done as sudo
  log_info "Enabling NTP"
  set_ini_value /etc/systemd/timesyncd.conf Time NTP pool.ntp.org
  set_ini_value /etc/systemd/timesyncd.conf Time FallbackNTP "0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org"
  systemctl enable systemd-timesyncd.service
  systemctl start systemd-timesyncd.service

  #localectl status
}
