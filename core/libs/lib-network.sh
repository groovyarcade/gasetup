#!/bin/bash

# configures network on host system according to installer settings
# if some variables are not set, we handle that transparantly
# however, at least $DHCP must be set, so we know what do to
# we assume that you checked whether networking has been setup before calling us
target_configure_network()
{
	# networking setup could have happened in a separate process (eg partial-configure-network),
	# so check if the settings file was created to be sure
	if [ -f $RUNTIME_DIR/aif-network-settings ]; then

        debug NETWORK "Configuring network settings on target system according to installer settings"

        source $RUNTIME_DIR/aif-network-settings 2>/dev/null || return 1

        IFO=${INTERFACE_PREV:-eth0} # old iface: a previously entered one, or the arch default
        IFN=${INTERFACE:-eth0} # new iface: a specified one, or the arch default

        # comment out any existing uncommented entries, whether specified by us, or arch defaults.
        for var in eth0 $IFO INTERFACES gateway ROUTES
        do
            sed -i "s/^$var=/#$var=/" ${var_TARGET_DIR}/etc/rc.conf || return 1
        done
        sed -i "s/^nameserver/#nameserver/" ${var_TARGET_DIR}/etc/resolv.conf || return 1
        if [ -f ${var_TARGET_DIR}/etc/profile.d/proxy.sh ]
        then
            sed -i "s/^export/#export/" ${var_TARGET_DIR}/etc/profile.d/proxy.sh || return 1
        fi

        if [ "$DHCP" = 0 ] ; then
            local line="$IFN=\"$IFN ${IPADDR:-192.168.0.2} netmask ${SUBNET:-255.255.255.0} broadcast ${BROADCAST:-192.168.0.255}\""
            append_after_last "/$IFO\|eth0/" "$line" ${var_TARGET_DIR}/etc/rc.conf || return 1

            if [ -n "$GW" ]; then
                append_after_last "/gateway/" "gateway=\"default gw $GW\"" ${var_TARGET_DIR}/etc/rc.conf || return 1
                append_after_last "/ROUTES/" "ROUTES=(gateway)" ${var_TARGET_DIR}/etc/rc.conf || return 1
            fi
            if [ -n "$DNS" ]
            then
                echo "nameserver $DNS" >> ${var_TARGET_DIR}/etc/resolv.conf || return 1
            fi
        else
            append_after_last "/$IFO\|eth0/" "$IFN=\"dhcp\"" ${var_TARGET_DIR}/etc/rc.conf || return 1
        fi

        append_after_last "/$IFO\|eth0/" "INTERFACES=($IFN)" ${var_TARGET_DIR}/etc/rc.conf || return 1

        if [ -n "$PROXY_HTTP" ]; then
            echo "export http_proxy=$PROXY_HTTP" >> ${var_TARGET_DIR}/etc/profile.d/proxy.sh || return 1
            chmod a+x ${var_TARGET_DIR}/etc/profile.d/proxy.sh || return 1
        fi

        if [ -n "$PROXY_FTP" ]; then
            echo "export ftp_proxy=$PROXY_FTP" >> ${var_TARGET_DIR}/etc/profile.d/proxy.sh || return 1
            chmod a+x ${var_TARGET_DIR}/etc/profile.d/proxy.sh || return 1
        fi
    else
        debug NETWORK "Skipping Host Network Configuration - aif-network-settings not found"
    fi
    return 0
}

# Returns a list of \n sparated wifi adapters
list_wifi_adapters() {
  iw dev | grep "Interface " | sed -E "s/[[:space:]]+Interface (.*)$/\1/g"
}

# List the SSIDs a wifi adapter can see
# Returns a \n separated SSIDs list
# $1 : the interface(ex: wlan0)
list_ssid() {
  local iface="$1"

  sudo iw dev "$iface" scan | grep "SSID:"| sed -E "s/[[:space:]]+SSID: (.*)$/\1/g" | sort | uniq | grep -v '^$'
}

# Try connecting to a SSID
# Returns 0 if success, 1 otherwise
# $1: interface (ex: wlan0)
# $2: SSID
# $3: password
connect_to_ssid() {
  local iface="$1"
  local ssid="$2"
  local passwd="$3"

  # If the SSID has no password, don't pass the argument
  if [[ -z $passwd ]] ; then
    iwctl station "$iface" connect "$ssid"
  else
    iwctl --passphrase "$passwd" station "$iface" connect "$ssid"
  fi
  return $?
}

# UI to select a SSID
# $1: a wifi adapter
# Returns 0 if a SSID was selected, otherwise 1
# The SSID is echoed on stdout
select_ssid() {
  local iface="$1"
  local list=
  local opts_list=()

  inform "Scanning SSIDs on adapter ${iface}..."
  list=$(list_ssid "$iface")
  local i=1
  IFS=$'\n'
  for a in $list ; do
    opts_list+=($i)
    opts_list+=("$a")
    i=$((i + 1))
  done
  unset IFS
  ask_option no "Select WiFi SSID" "Choose a SSID in the list" required "${opts_list[@]}"
  [[ $? == 0 ]] || return 1
  echo "$list" | sed -n "${ANSWER_OPTION}p"
  return 0
}

# Connect using WPS. Can take up to 2 minutes in case of failure
# $1: the network interface
connec_to_ssid_with_wps() {
  local iface="$1"
  iwctl wsc "$iface" push-button
  return $?
}

# Check if one need to set the regulatory domain
# Returns 1 if the regulatory domain is not set, 0 otherwise
check_if_need_set_regulatory_domain() {
  local current_regdom=$(iw reg get | grep -o "country ..")
  if [[ $current_regdom == "country 00" ]] ; then
    return 1
  else
    return 0
  fi
}

# Sets the regulatory domain
# $1: the regdom
set_regulatory_domain() {
  local regdom="$1"

  # check the regulatory domain exists
  if ! grep 'WIRELESS_REGDOM="'$regdom'"' /etc/conf.d/wireless-regdom ; then
    echo "The regulatory domain  $regdom is not valid!"
    return 1
  fi

  # Comment the curent one
  sudo sed -Ei "s/^WIRELESS_REGDOM=(.*)$/#WIRELESS_REGDOM=\1/" /etc/conf.d/wireless-regdom
  # Set the new one
  sudo sed -Ei "s/^#(WIRELESS_REGDOM=${regdom})$/\1/" /etc/conf.d/wireless-regdom
  # Apply !
  sudo iw reg set "$regdom"
}

# Returns the list of possible regulatory domains
get_regulatory_domain_list() {
  grep "WIRELESS_REGDOM=" /etc/conf.d/wireless-regdom | cut -d '"' -f 2
}

# UI to let the user select a regulatory domain
# returns 0 if a regdom was set, 1 otherwise
select_regulatory_domain() {
  local list="$(get_regulatory_domain_list)"
  local i=1
  opts_list="$(for a in $list ; do echo -n "$i $a " ; i=$((i + 1)) ; done)"
  ask_option no "Select a regulatory domain" "The WiFi frequencies allowed depend on countries" required $opts_list
  [[ $? == 0 ]] || return 1
  echo "$list" | sed -n "${ANSWER_OPTION}p"
  return 0
}

# UI to select the wifi adapter
# Returns 1 if an adapter was selected, oterwise 1
# The adapter is echoed to stdout
select_wifi_interface() {
  local list="$(list_wifi_adapters)"
  local num_adapters="$(echo "$list" | wc -l)"

  # If no adapter was found, don't go further
  if [[ -z $list ]] ; then
    notify "No WiFi interface found. You may need additional drivers."
    return 1
  # If there is only one adapter, keep it and go to the next dialog
  elif [[ $num_adapters == 1 ]] ; then
    iface="$(echo $list |tr -d '\n')"
  # Choose the right interface
  else
    local i=1
    opts_list="$(for a in $list ; do echo -n "$i $a " ; i=$((i + 1)) ; done)"
    ask_option no "Select a WiFi adapter" "Choose a WiFi adapter in the list" required $opts_list
    [[ $? == 0 ]] || return 1
    iface=$(echo $list | sed -n "${ANSWER_OPTION}p")
  fi

  echo $iface
  return 0
}

worker_configure_wifi() {
  # Get the interface first
  local iface=$(select_wifi_interface)
  local ssid=
  [[ $? == 1 ]] && return 1 # No wifi card, we're done here

  # Check if a regulatory domain is set
  if ! check_if_need_set_regulatory_domain ; then
    # Not set, ask the user
    regdom=$(select_regulatory_domain)
    [[ $? == 1 ]] && return 1 # No regdom was decided
    set_regulatory_domain "$regdom"
  fi

  # Ask if WPS is supported
  if ask_yesno "Do you want to connect using WPS?" ; then
    # YES -> then go that way, the procedure will end after that
    # Need a message here like when the keyboard is set
    inform "Waiting for a WPS connection..."
    connec_to_ssid_with_wps "$iface"
    if [[ $? != 0 ]] ; then
      notify "Couldn't connect using WPS"
      return 1
    fi
    local ssid=$(iw dev "$iface" link | grep "SSID:"| sed -E "s/[[:space:]]+SSID: (.*)$/\1/g")
    notify "Successfully connected to SSID: $ssid"
    return 0
  fi

  # Not going WPS -> ask a SSID
  ssid=$(select_ssid "$iface")
  [[ $? == 0 ]] || return 1 # No SSID selected
  # Ask a password
  ask_string "Type the password for SSID $ssid" || return 1
  local passwd="$ANSWER_STRING"
  # test connecting
  inform "Trying to connect to SSID ${ssid}..."
  connect_to_ssid "$iface" "$ssid" "$passwd"
  if [[ $? != 0 ]] ; then
    notify "Couldn't connect to $ssid"
    return 1
  fi
  notify "Success!"
  return 0

}
