#!/bin/bash
export GA_HOME="$(getent passwd "$GA_USER" | cut -d: -f6)"
source /opt/gasetup/core/configs/includes.sh
RA_SYSTEM_LIST=/opt/gasetup/core/configs/retroarch/ra.inc.sh
source "$RA_SYSTEM_LIST"
declare -gA RA_AVAILABLE_SYSTEMS

ra_pkglist=(retroarch libretro-core-info)

ra_check_required_packages() {
  for p in ${ra_pkglist[@]} ; do
    pacman -Qi "$p" &>/dev/null || return 1
  done
  return 0
}

ra_install_required_packages() {
  #sudo pacman -Sy --noconfirm ${ra_pkglist[@]}
  ask_yesno "You must first make a full system update first. Do it?" ||return 1
  execute worker system_full_update
  ask_yesno "Some packages for Retroarch are missing. Install them?" || return 1
  log_info "RETROARCH: installing the libretro group"
  sudo pacman -Sy --noconfirm --needed libretro ||return 1

  return $?
}

ra_set_missing_default_cores() {
  inform "Setting default cores..."
  for t in "${!RA_AVAILABLE_SYSTEMS[@]}" ; do
    # Only keep systems that have several possible cores"
    echo "${RA_AVAILABLE_SYSTEMS[$t]}" | grep -q "|" && continue
    # Only keep cores that can be mapped to a folder name
    [[ -z ${_system_folder[$t]} ]] && continue
    # We have a single core system, let's check if it exists
    def_core="$(get_config_value "$GA_CONF" retroarch.${_system_folder[$t]}.core)"
    if [[ -z $def_core ]] ; then
      set_config_value "$GA_CONF" retroarch.${_system_folder[$t]}.core ${RA_AVAILABLE_SYSTEMS[$t]}
      ra_filter_systems "$t"
    fi
  done
}


# For system $1, check if at least 1 core is installed
ra_check_system_has_installed_core() {
  local core_path="$(get_ra_config_value libretro_directory)"
  IFS="|"
  for c in $1 ; do
    [[ -e "$core_path"/"$c"_libretro.so ]] && return 0
  done
  return 1
}


# Set RA to power user mode where the user can download cores
#   - set the right cores path -> libretro_directory = /home/arcade/shared/configs/retroarch/cores
#   - show the update menu -> menu_show_core_updater = true
ra_set_power_mode() {
  log_info "RETROARCH: setting to power user mode"
  set_ra_config_value libretro_directory "$MOTHER_OF_ALL"/configs/retroarch/cores
  set_ra_config_value libretro_info_path "$MOTHER_OF_ALL"/configs/retroarch/cores
  set_ra_config_value menu_show_core_updater true
  if ask_yesno "Do you want to download cores now ?" ; then
    execute worker ra_download_all_cores_from_buildbot
  else
    notify "Do not forget to 'Download all cores'"
  fi
}


# Set RA to the default ARCH mode
ra_set_default_mode() {
  log_info "RETROARCH: setting to default user mode"
  set_ra_config_value libretro_directory /usr/lib/libretro
  set_ra_config_value libretro_info_path /usr/share/libretro/info
  set_ra_config_value menu_show_core_updater false

  # Make sure the libretro group is installed
  for p in $(pacman -Sqg libretro) ; do
    if ! pacman -Qi "$p" &>/dev/null ; then
      inform "Some packages are missing"
      sleep 2
      ra_install_required_packages
      return $?
    fi
  done

  return 0
}


ra_extra_config() {
  local tmpcfg=/tmp/autoconfig.zip
  local autocfg_url="https://buildbot.libretro.com/assets/frontend/autoconfig.zip"
  local autocfg_dir="$(get_ra_config_value joypad_autoconfig_dir)"

  # Now download the joypad autoconfig files
  inform "Downloading and extracting joypad autoconfig file..."
  log_info "Downloading and extracting joypad autoconfig file..."
  curl -L -o "$tmpcfg" "$autocfg_url"
  if [[ $? != 0 ]] ; then
    inform "Something wrong happened while downloading. Aborting."
    sleep 2
    rm "$tmpcfg"
    return 1
  fi
  unzip -o "$tmpcfg" -d "$autocfg_dir"
  chown -R "$GA_USER":"$GA_GROUP" "$autocfg_dir"
  rm "$tmpcfg"
}

ra_prepare_available_systems() {
  unset RA_AVAILABLE_SYSTEMS
  declare -gA RA_AVAILABLE_SYSTEMS
  local info_path="$(get_ra_config_value libretro_info_path)"
  local core_path="$(get_ra_config_value libretro_directory)"

  inform "Preparing cores list..."
  for f in "$info_path"/*.info ; do
    # Check if the core exists, skip otherwise
    core_name="$(basename $f _libretro.info)"
    core_file="$core_path"/"$core_name"_libretro.so
    [[ ! -e ${core_file} ]] && continue
    # Now we can read the system
    db="$(get_ra_config_value "$f" database)"
    [[ -z $db ]] && db="$(get_ra_config_value "$f" systemname)"
    [[ -z $db ]] && db="$(get_ra_config_value "$f" corename)"

    # Parse all possible systems
    # Get the exact system name
    IFS="|"
    for l in $db ; do
      [[ -n ${_system_names_fix["$l"]} ]] && l=${_system_names_fix["$l"]}
      if [[ -n  "${RA_AVAILABLE_SYSTEMS[$l]}" ]] ; then
        # Don't add twice
        RA_AVAILABLE_SYSTEMS["$l"]="${RA_AVAILABLE_SYSTEMS[$l]}|$core_name"
      else
        RA_AVAILABLE_SYSTEMS["$l"]="$core_name"
      fi
      RA_AVAILABLE_SYSTEMS["$l"]=$(echo "${RA_AVAILABLE_SYSTEMS[$l]}" | tr '|' '\n' | sort -u | paste -sd "|" -)
    done
    unset IFS
  done
}

ra_filter_systems() {
  for f in /home/arcade/shared/frontends/* ; do
    fe="$(basename "$f")"
    [[ $(type -t ra_filter_systems_${fe}) == function ]] && ra_filter_systems_${fe} "$1"
  done
}

ra_filter_systems_attract() {
  # From the list of possible systems vs every possible systems, just keep those
  # for which we have a core
  forced_system="$1"
  log_info "Configuring AM for Retroarch ($forced_system)"
  #env ; set
  # Need to optimize here, and not loop through the whole _system_folder
  # when a system was forced
  for s in "${!_system_folder[@]}" ; do
    local em_cfg="/home/arcade/shared/frontends/attract/emulators/${s}.cfg"
    local efc_src="/opt/gasetup/core/configs/retroarch/efc/$s".efc
    [[ -n $forced_system && "$s" != "$forced_system" ]] && continue
    # The system has no core, remove the .cfg file
    if [[ -z RA_AVAILABLE_SYSTEMS["$s"] ]] ; then
      if [[ -e $em_cfg ]] ; then
        log_debug "Removing $em_cfg"
        rm "$em_cfg"
      fi
    else
    # The system has a core, copy the .cfg file
      if [[ -e $efc_src ]] && ra_check_system_has_installed_core ${RA_AVAILABLE_SYSTEMS["$s"]} ; then
        log_debug "Installing $efc_src to $em_cfg"
        # This is really ugly, but $GA_FRONTENDS misses the $GA_HOME part when source from core/configs/includes.sh
        GA_USER=$GA_USER GA_GROUP=$GA_GROUP GA_FRONTENDS="$GA_HOME"/shared/frontends /opt/gatools/efc/efc.sh attract "$efc_src"
      else
        log_ko "Not installing '$s' to AM"
      fi
    fi
  done
}


worker_setup_retroarch() {
  if ! ra_check_required_packages ; then
    inform "Setting up GroovyArcade for Retraorch..."
    ra_install_required_packages
    assert_info "Could not install Retroarch required packages. Aborting!" test "$?" -eq 0 || return 1
    sudo /opt/gasetup/gasetup.sh -p interactive -c retroarch -u "$GA_USER"
    set_ra_config_value joypad_autoconfig_dir "$MOTHER_OF_ALL"/configs/retroarch/autoconfig
    ra_prepare_available_systems
    ra_filter_systems
    ra_set_missing_default_cores
    ra_extra_config
  else
    ra_prepare_available_systems
  fi

  while true ; do
    # The Download all cores is only available to advanced mode
    if [[ $(get_ra_config_value libretro_directory) != /usr/lib/libretro ]] ; then
      ask_option no "Retroarch" "Choose an option" required \
        1 "Set default core" \
        2 "Normal/Advanced configuration" \
        3 "Download all cores" \
        4 "Return to previous menu" || return
    else
      ask_option no "Retroarch" "Choose an option" required \
        1 "Set default core" \
        2 "Normal/Advanced configuration" \
        4 "Return to previous menu" || return
    fi

    case $ANSWER_OPTION in
      "1") execute worker ra_choose_core ;;
      "2") execute worker ra_set_mode ;;
      "3") execute worker ra_download_all_cores_from_buildbot ;;
      "4") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}


worker_ra_choose_core() {
  inform "Building the systems list..."
  assert_info "No core found, please reinstall retroarch cores" test "${#RA_AVAILABLE_SYSTEMS[@]}" -gt 0 ||return 1

  # Now construct the UI to select a system
  local i=1
  local prm=()
  ## Let's first sort the systems list
  readarray -t sorted < <(for a in "${!RA_AVAILABLE_SYSTEMS[@]}"; do echo "$a"; done | sort)
  ## Now make a list for ask_option
  for t in "${sorted[@]}" ; do
    # Only keep systems that have several possible cores"
    echo "${RA_AVAILABLE_SYSTEMS[$t]}" | grep -q "|" || continue
    # Only keep cores that can be mapped to a folder name
    [[ -z ${_system_folder[$t]} ]] && continue
    prm+=($i "$t")
    ((i++))
  done
  prm+=($i "Return to previous menu")
  leave=$i # This one is the "Return to menu" value
  def=no   # This will then be the last system selected for the systems list

  while true ; do
    ask_option $def "Default core" "Select a system to configure its default core" required "${prm[@]}"
    [[ -z $ANSWER_OPTION || $ANSWER_OPTION == $leave ]] && return 1
    local idx=$((ANSWER_OPTION*2-1))
    local syscore="${prm[$idx]}"
    def="$ANSWER_OPTION"
    def=no

    # Now show an UI with the list of possible cores
    i=1
    clst=()
    IFS="|"
    for s in ${RA_AVAILABLE_SYSTEMS[$syscore]} ; do
      clst+=($i "$s")
      ((i++))
    done
    unset IFS
    ask_option no "$syscore" "Select the default core" required "${clst[@]}"
    [[ -z $ANSWER_OPTION ]] && continue
    idx=$((ANSWER_OPTION*2-1))
    local core="${clst[$idx]}"
    assert_info "Couldn't detect the system name for system $syscore, failed." test -n "${_system_folder[$syscore]}" || continue
    set_config_value "$GA_CONF" retroarch.${_system_folder[$syscore]}.core "$core"
    ra_filter_systems "$syscore"
  done
}


worker_ra_set_mode() {
  local default=no
  while true ; do
    ask_option $default "Retroarch" "Choose an option" required \
      1 "Default mode" \
      2 "Power user mode" \
      3 "Return to previous menu" || return

    case $ANSWER_OPTION in
      "1")
        ask_yesno "Uses the Arch linux default, the core updater menu is disabled. Safe, but limited. Good for beginners."
        [[ $? == 0 ]] && ra_set_default_mode
        default=3 ;;
      "2")
        ask_yesno "For people with a good knowledge of Retroarch. You can use the core updater."
        [[ $? == 0 ]] && ra_set_power_mode
        default=3 ;;
      "3") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}


worker_ra_download_all_cores_from_buildbot () {
  local arch="$(uname -m)"
  local ra_version="$(pacman -Qi retroarch | grep Version | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")"
  local tmpdl=/tmp/cores.7z
  local tmpnfo=/tmp/info.zip
  local tmpcfg=/tmp/autoconfig.zip
  local info_url="https://buildbot.libretro.com/assets/frontend/info.zip"
  local autocfg_url="https://buildbot.libretro.com/assets/frontend/autoconfig.zip"
  local dst_dir="$(get_ra_config_value libretro_directory)"
  local nfo_dir="$(get_ra_config_value libretro_info_path)"
  local autocfg_dir="$(get_ra_config_value joypad_autoconfig_dir)"

  # Ask to download from nightly or stable
  ask_option no "Core downloader" "Choose a source for cores" required 1 Stable 2 Nightly
  [[ $? != 0 ]] && return 1
  case $ANSWER_OPTION in
    "1") core_source=stable/${ra_version} ;;
    "2") core_source=nightly ;;
  esac
  # ARM has no stable release
  # stable ->  https://buildbot.libretro.com/stable/${ra_version}/linux/${arch}/RetroArch_cores.7z"
  # nightly -> https://buildbot.libretro.com/nightly/linux/${arch}/RetroArch_cores.7z
  local download_url="https://buildbot.libretro.com/$core_source/linux/${arch}/RetroArch_cores.7z"

  local file_size="$(curl -sI "$download_url" | grep -i Content-Length | grep -oE "[0-9]+")"
  assert_info "Couldn't locate the source URL, aborting" test "$?" -eq 0 || return 1
  assert_info "No cores for retroarch $core_source, aborting" test -n "$file_size" || return 1

  ask_yesno "The file is '$file_size' bytes. OK to download?"
  [[ $? != 0 ]] && return 1

  # Make sure /tmp has the required space
  # Ask if it's ok to download ?
  # Download
  log_info "RETROARCH: downloading all cores"
  wget -O "$tmpdl" "$download_url" 2>&1 | stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }' |  dialog --gauge "Downloading cores" 10 50
  # $? !=0 -> failed, so erase the file and cancel
  if [[ $? != 0 ]] ; then
    inform "Something wrong happened while downloading. Aborting."
    sleep 2
    rm "$tmpdl"
    return 1
  fi

  # Uncompress
  inform "Extracting the cores... Be patient!"
  7z e "$tmpdl" -o"$dst_dir" -y "RetroArch-Linux-${arch}/RetroArch-Linux-${arch}.AppImage.home/.config/retroarch/cores/*_libretro.so"
  # Delete the file
  rm "$tmpdl"

  # Now download the info files
  inform "Downloading and extracting infos file..."
  log_info "Downloading and extracting infos file..."
  curl -L -o "$tmpnfo" "$info_url"
  if [[ $? != 0 ]] ; then
    inform "Something wrong happened while downloading. Aborting."
    sleep 2
    rm "$tmpnfo"
    return 1
  fi
  unzip -o "$tmpnfo" -d "$nfo_dir"
  chown -R "$GA_USER":"$GA_GROUP" "$dst_dir" "$nfo_dir"
  rm "$tmpnfo"

  inform "Updating frontends..."
  ra_prepare_available_systems
  assert_info "No core found, please reinstall retroarch cores" test "${#RA_AVAILABLE_SYSTEMS[@]}" -gt 0 ||return 1

  # Set systems that have a single core. Quite useful after a "Download all cores"
  ra_set_missing_default_cores
  ra_filter_systems

  # Warn the user he has to set some default cores
  notify "You now have a number of systems that have at least 2 possible cores".
  notify "You can configure them in 'Set default core'. This will update the frontend at the same time"
  return 0
}
