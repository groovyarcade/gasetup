#!/bin/bash

# gatools should already be included. This shell is not made to be called as a standalone

LIB_SYSTEM=1


worker_system_packages_menu() {
  local default=1
  local monitor
  # shellcheck disable=SC2034
  VIDEO_SETUP=1
  while true ; do
    # Keeping only options that are useful and have been tested
    ask_option $default "Softwares management" "Select an option" required \
      1 "Install/Uninstall" \
      2 "GroovyArcade specific Update" \
      3 "Full OS update" \
      4 "Enable/Disable testing repo" \
      5 "Freeze specific packages updates" \
      9 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") execute worker system_packages_install ;;
      "2") execute worker system_packages_update_ga_only ;;
      "3") execute worker system_full_update ;;
      "4") execute worker manage_groovyarcade_testing_repo ;;
      "5") execute worker freeze_updates ;;
      "9") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
    # Sub menus will have their own $ANSWER_OPTION, but this variable is global
    # So $ANSWER_OPTION may not be a number and make an error here
    default=$((ANSWER_OPTION + 1))
  done

  return 0
}


update_pacman_db() {
  inform "Updating packages database"
  sudo pacman -Sy &>/dev/null
  if [[ $? != 0 ]] ; then
    inform "Couldn't update the package database. Aborting."
    sleep 2
    return 1
  fi
  return 0
}


worker_system_packages_install() {
  # 1. List existing packages
  # 2. set which ones are installed
  # 3. Ask user
  inform "Coming soon!"
  sleep 2
  return 0
}

# Let's not use this one for now, it lists GA specific updatable packages and lets
# the user decide which ones to update. The'yll make errors for sure, so I keep
# the code for now as it is useful, but not for that purpose yet
worker_system_packages_update_checklist() {
  # 1. update pacman
  # 2. copmpare the list of updatable packages against groovy installed packages
  # 3. ask user

  #
  # 1. Inform the user we're updating the pacman DB
  #
  update_pacman_db ||return 1

  #
  # 2. compare the list of updatable packages against groovy installed packages
  #
  local repo=groovyarcade
  local installed_pkgs=
  local updatable_pkgs=()

  # Check if the testing repo is active
  grep -qE '^\[groovyarcade-testing]$' /etc/pacman.d/groovy-ux-repo.conf && repo="groovyarcade groovyarcade-testing"
  for r in $repo ; do
    # Line example:
    # groovyarcade groovymame 0.227-1 [installed: 0.228-1]
    # |->repo      |-> package
    installed_pkgs="$(LANG=C pacman -Sl "$r" | grep "installed: " | cut -d ' ' -f2) $installed_pkgs"
  done

  # sort and deduplicate
  installed_pkgs="$(echo "$installed_pkgs" | sort | uniq)"
  # If the list is empty, inform and leave
  if [[ -z $installed_pkgs ]] ; then
    inform "No package to update"
    sleep 2
    return 0
  fi

  # Select updatable packages
  IFS=$'\n'
  for f in $(pacman -Qu) ; do
    # Check if the current package is in our list
    p="$(echo $f | cut -d ' ' -f1)"
    details="$(echo $f | cut -d ' ' -f2-)"
    echo "$installed_pkgs" | grep -q "$p" || continue
    updatable_pkgs+=($p "$details" OFF)
  done
  unset IFS

  #
  # 3. Display the list
  #
  ask_checklist "Select packages to update" 0 "${updatable_pkgs[@]}"
  log_info "About to update ${ANSWER_CHECKLIST[@]}"
  [[ -z ${ANSWER_CHECKLIST[@]} ]] && return 1
  sudo pacman -S --noconfirm ${ANSWER_CHECKLIST[@]}
  if [[ $? != 0 ]] ; then
    log_ko "Update failed"
    inform "Something went wrong when updating. Aborting."
    sleep 2
    return 1
  fi
  log_ok "Update succeeded"
  return 0
}


worker_system_packages_update_ga_only() {
  # 1. update pacman
  # 2. copmpare the list of updatable packages against groovy installed packages
  # 3. prompt user

  #
  # 1. Inform the user we're updating the pacman DB
  #
  update_pacman_db ||return 1

  #
  # 2. compare the list of updatable packages against groovy installed packages
  #
  local repo=groovyarcade
  local installed_pkgs=
  local updatable_pkgs=()

  # Check if the testing repo is active
  if grep -qE '^\[groovyarcade-testing]$' /etc/pacman.d/groovy-ux-repo.conf ; then
    inform "The GroovyArcade testing repository is enabled. Only update from it."
    sleep 2
    repo="groovyarcade-testing"
  fi

  # Get the list of updatable packages
  # Line example:
  # groovyarcade groovymame 0.227-1 [installed: 0.228-1]
  # |->repo      |-> package
  installed_pkgs="$(LANG=C pacman -Sl "$repo" | grep "installed: " | cut -d ' ' -f2)"

  # sort and deduplicate
  installed_pkgs="$(echo "$installed_pkgs" | sort | uniq)"
  # If the list is empty, inform and leave
  if [[ -z $installed_pkgs ]] ; then
    inform "No package to update"
    sleep 2
    return 0
  fi

  update_groovyarcade $installed_pkgs
  return $?
}


worker_system_full_update() {
  update_pacman_db ||return 1

  if ! sudo pacman -Qu ; then
    inform "No package to update"
    sleep 2
    return 0
  fi
  update_groovyarcade

  return $?
}


worker_manage_groovyarcade_testing_repo() {
  local next_status=disable

  grep -o "#\[groovyarcade-testing\]$" /etc/pacman.d/groovy-ux-repo.conf && next_status=enable

  ask_yesno "Do you really want to $next_status the testing repo ? Use at your own risks" || return 1

  if [[ $next_status == "enable" ]] ; then
    # Enable
    sudo sed -Ei '1,3s/^#(.*)/\1/g' /etc/pacman.d/groovy-ux-repo.conf
  else
    # Disable
    sudo sed -Ei '1,3s/(.*)/#\1/g' /etc/pacman.d/groovy-ux-repo.conf
  fi
  return $?
}


worker_system_install_gnome_software() {
  # Warn that groovy specific packages are not listed there
  sudo pacman -S gnome-software-packagekit-plugin archlinux-appstream-data
}


update_groovyarcade() {
  notify "$(sudo pacman -Qu $@ | sort | uniq)" || return 2

  ask_yesno "Do you really want to update ?" || return 3

  local pacman_opts="-Su --noconfirm"

  [[ ! -z "$@" ]] && pacman_opts="-Sy --noconfirm $@"

  # First update the keyring, easy problem source to fix
  sudo pacman -Sy --needed --noconfirm archlinux-keyring gasetup

  if ! sudo pacman $pacman_opts ; then
    notify "You'll have to manually run 'sudo pacman -Syu $@' and answer questions. Grab your keyboard !"
    return 4
  fi

  # Fix FS#70469 as Arch is not doing anything
  sudo chmod +s /usr/lib/Xorg.wrap
  return 0
}


pkg_freeze() {
  local pkg="$1"
  local currently_freezed_packages="$(get_config_value /etc/pacman.conf IgnorePkg '[[:space:]]*= ' | xargs)"
  [[ $currently_freezed_packages == *"$pkg"* ]] && return 0
  set_pacman_config_value IgnorePkg "$currently_freezed_packages $pkg"
}

pkg_unfreeze () {
  local pkg="$1"
  local currently_freezed_packages="$(get_config_value /etc/pacman.conf IgnorePkg '[[:space:]]*= ')"
  [[ $currently_freezed_packages != *"$pkg"* ]] && return 0
  # xargs is just here to trim leading and traiilng whitespeaces
  new_pkg_list="$(echo $currently_freezed_packages | sed "s/$pkg//" | xargs)"
  set_pacman_config_value IgnorePkg "$new_pkg_list"
}

worker_freeze_updates() {
  # This list can have real package names, r the name of a group which would refer
  # to a sublist of package. See kernel and kernel_pkgs
  local freezable_pkgs="kernel groovymame"
  local groovymame_pkgs="groovymame groovymame-extra"
  local kernel_pkgs="linux-15khz linux-15khz-headers linux-15khz-docs linux-lts-15khz linux-lts-15khz-headers linux-lts-15khz-docs"
  local expanded_pkgs=
  local currently_freezed_packages=

  # make the list of real package names
  for p in $freezable_pkgs ; do
    group_name=${p}_pkgs
    if [[ -n ${!group_name} ]] ; then
      for pp in ${!group_name} ; do
        expanded_pkgs+="$pp "
      done
    else
      expanded_pkgs+="$p "
    fi
  done


  # Read the currently ignored packaages
  currently_freezed_packages="$(get_config_value /etc/pacman.conf IgnorePkg '[[:space:]]*= ')"

  # Check if a freezable one is enabled
  local pkg_list=""
  local skipped_group=
  for p in $freezable_pkgs ; do
    group_name=${p}_pkgs
    echo $p
    if [[ -n ${!group_name} ]] ; then
      for pp in ${!group_name} ; do
        echo $pp
        if [[ "$currently_freezed_packages" == *"$pp"* ]] ; then
          pkg_list+="$p ^ ON "
          skipped_group=1
          break
        fi
      done
      [[ -z $skipped_group ]] && pkg_list+="$p - OFF "
      skipped_group=
    else
      if [[ "$currently_freezed_packages" == *"$p"* ]] ; then
          pkg_list+="$p ^ ON "
      else
          pkg_list+="$p - OFF "
      fi
    fi
  done

  # Prepare the dialog
  ask_checklist "Select packages to freeze:" 0 $pkg_list

  # for each value from the dialog, (un)freeze packages
  for p in $freezable_pkgs ; do
    pkg_cmd=
    if [[ "${ANSWER_CHECKLIST[@]}" == *"$p"* ]] ; then
      pkg_cmd=pkg_freeze
    else
      pkg_cmd=pkg_unfreeze
    fi

    group_name=${p}_pkgs

    if [[ -n ${!group_name} ]] ; then
      for pp in ${!group_name} ; do
        $pkg_cmd "$pp"
      done
    else
      $pkg_cmd "$p"
    fi
  done
set +x
}

worker_select_kernel() {
  local possible_kernels="linux-15khz linux-lts-15khz linux-rt-15khz"
  local current_kernel="$(grep -o 'linux-.*-15khz' /proc/version)"
  current_kernel="${current_kernel:-linux-15khz}"
  local selected_kernel=""
  local opts_list=()
  local i=1

  for a in $possible_kernels ; do
    opts_list+=($i)
    opts_list+=("$a")
    i=$((i + 1))
  done

  ask_option "$current_kernel" "Select a kernel" "LTS stands for long term support are considered more stable" required "${opts_list[@]}"
  if [[ -z $ANSWER_OPTION ]] ; then
    inform "No valid kernel selected" ; sleep 2
    return 1
  fi

  selected_kernel="$(echo "$possible_kernels" | cut -d ' ' -f "$ANSWER_OPTION")"
  # Shall we download the kernel ?
  pacman -Sy --needed --noconfirm "$selected_kernel" "$selected_kernel"-headers "$selected_kernel"-docs
  set_config_value "$GA_CONF" kernel "$selected_kernel"

  # Keep the default, remove fallback
  sed -i "s/^PRESETS=.*/PRESETS=('default')/" /etc/mkinitcpio.d/"$selected_kernel".preset

  log_info "Setting kernel to $selected_kernel and configuring bootloader"
  configure_boot_loader
}
