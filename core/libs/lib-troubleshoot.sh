#!/bin/bash

debug_enable() {
  set_ra_config_value libretro_log_level 0
  set_ra_config_value log_verbosity true
  set_mame_config_value verbose 1
  set_mame_config_value "$SWITCHRES_INI" verbosity 3
  set_config_value "$GA_CONF" verbose 1
}

debug_disable() {
  set_ra_config_value libretro_log_level 1
  set_ra_config_value log_verbosity false
  set_mame_config_value verbose 0
  set_mame_config_value "$SWITCHRES_INI" verbosity 2
  set_config_value "$GA_CONF" verbose 0
}

worker_troubleshoot_logging() {
  ask_option $default "Select an action" "Troubleshoot" required \
      1 "Set debug log level" \
      2 "Set normal log level" \
      3 "Return to Main" || return

  case $ANSWER_OPTION in
    "1") debug_enable ;;
    "2") debug_disable ;;
    "3") return 0 ;;
    *) notify "Bad option $ANSWER_OPTION given" ;;
  esac

}

worker_troubleshoot_menu () {
  while true ; do
    ask_option $default "Select an action" "Troubleshoot" required \
      1 "Send logs" \
      2 "Logging level" \
      3 "Return to Main" || return

    case $ANSWER_OPTION in
      "1")
          url="$(bash /opt/gatools/support/support.sh)"
          notify "Please report this url: $url"
          default=2
          ;;
      "2") execute worker troubleshoot_logging
           default=3
          ;;
      "3") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}

worker_rescue_menu() {
  ask_option $default "Select an action" "Troubleshoot" required \
      1 "Log in your GroovyArcade install" \
      2 "Fix UEFI boot" \
      3 "Return to Main" || return

  case $ANSWER_OPTION in
    "1") execute worker rescue_mode ;;
    "2") execute worker fix_uefi_boot ;;
    "3") return 0 ;;
    *) notify "Bad option $ANSWER_OPTION given" ;;
  esac
}

worker_rescue_mode() {
  # Information message
  ask_yesno "This mode will log you as root on the local GroovyArcade installation. Continue ?" || return

  # Find the right installation
  local ga_dev="$(findfs LABEL=GA)"
  local boot_dev="$(findfs LABEL=GABOOT)"

  # Warn we don't support yet multiple GA installation
  nb_devs="$(echo $ga_dev | wc -l)"
  nb_boot_devs="$(echo $boot_dev | wc -l)"
  if [[ $nb_devs -gt 1 ]] || [[ $nb_boot_devs -gt 1 ]] ; then
    # Nope, not yet done
    return
  fi

  # Remove anything mounted at /groovyarcade
  [[ ! -d /groovyarcade ]] && mkdir /groovyarcade
  # Reverse the sort to make sure /groovyarcade/boot comes out first
  for m in $(findmnt -lo target | grep "/groovyarcade" | sort -r) ; do
    umount "$m"
  done

  # Mount / then /boot
  mount "$ga_dev" /groovyarcade
  mount "$boot_dev" /groovyarcade/boot

  # arch-chroot
  clear
  arch-chroot /groovyarcade /bin/bash
  echo "arch-chroot a renvoyé : $?"

  # clean mounts
  umount /groovyarcade/boot
  umount /groovyarcade

  # For some unknown reason, ncurses won't work after arch-chroot and considers the current
  # job is backgrounded. So when returning to the menu, the process will be a STOPPED job
  # So better exit here and ask the user to run gasetup
  echo
  echo "To get back to the setup menu, run:"
  echo sudo gasetup
  echo
  exit
}

worker_fix_uefi_boot() {
  # Right now, this is rather ugly, until lib-bootloaders is fixed
  nb_ga_uefi_entries="$(efibootmgr | grep -E "(GroovyArcade|GAUEFI)" | wc -l)"
  if [[ $nb_ga_uefi_entries -gt 0 ]] ; then
    ask_yesno "There is already a GroovyArcade UEFI entry. ontinue ?" no || return 1
  fi

  # If several GA installs were found, things will become much more complicated, skip that for now
  nb_ga_installs="$(findfs LABEL=GA | wc -l)"
  if [[ $nb_ga_installs -gt 1 ]] ; then
    inform "Several GroovyArcade installations detected, this helper can't manage that for now. Aborting"
    sleep 2
    return 1
  fi

  sudo efibootmgr --create --disk /dev/"$(lsblk -no PKNAME /dev/disk/by-label/GA)" --part 1 --loader /EFI/syslinux/syslinux.efi --label "GroovyArcade UEFI" --verbose
  if [[ $? == 0 ]] ; then
    inform "A new UEFI entry for GroovyArcade was added"
  else
    warning "Couldn't add entry for GroovyArcade."
  fi
  sleep 2
  return 0
  
}
