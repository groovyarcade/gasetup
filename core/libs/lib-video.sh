#!/bin/bash

# gatools should already be included. This shell is not made to be called as a standalone

LIB_VIDEO=1
[[ -z VIDEO_DATA ]] && GA_VIDEO_DATA=/opt/galauncher/videodata.conf

worker_video_menu() {
  local default=1
  local monitor
  # shellcheck disable=SC2034
  VIDEO_SETUP=1
  while true ; do
    #~ ask_option $default "Video Setup Menu" "Setup video" required \
      #~ 1 "Monitor Type" \
      #~ 2 "Monitor Orientation" \
      #~ 3 "Monitor Aspect" \
      #~ 4 "Force Aspect for Vertical Games" \
      #~ 5 "GLSL CRT Emulation on LCD Screen" \
      #~ 6 "Video Boot Options" \
      #~ 7 "Custom & Presets Video Mode" \
      #~ 8 "Fix Video" \
      #~ 9 "Return to Main" || return

    # Keeping only options that are useful and have been tested
    ask_option $default "Video Setup Menu" "Setup video" required \
      1 "Monitor Type" \
      2 "Monitor Orientation" \
      6 "Video resolution" \
      9 "X/KMS" \
      10 "Tweak geometry" \
      11 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") execute worker select_monitor ;;
      "2") execute worker select_monitor_orientation ;;
      "3") execute worker select_monitor_aspect ;;
      "4") execute worker select_force_aspect_vertical ;;
      "5") execute worker select_monitor_crtlcd ;;
      "6") execute worker kernel_video_boot ;;
      "7") execute worker video_custom ;;
      "8") execute worker fix_video ;;
      "9") execute worker select_video_backend ; default=10 ;;
      "10") execute worker adjust_geometry ; default=11 ;;
      "11") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
    # Sub menus will have their own $ANSWER_OPTION, but this variable is global
    # So $ANSWER_OPTION may not be a number and make an error here
    default=9
  done

  return 0
}


worker_select_monitor() {
  source "$GAT_PATH"/video/video.sh
  monitor=$(menu_select_monitor)
  [[ $? == 0 ]] && set_monitor "$monitor"
}


worker_select_monitor_orientation () {
  local default=no
  local ORIENTATION=
  local flipx=0
  local flipy=0

  # Orientation
  ask_option $default "Monitor Orientation" "What orientation is your monitor?" required \
    "0" "Horizontal" \
    "1" "Rotated Right" \
    "2" "Rotated Left" \
    "3" "Flipped" \
    "4" "Rotating" \
    || return 1

  MO=$ANSWER_OPTION
  [[ "$MO" = 0 ]] && ORIENTATION=horizontal
  [[ "$MO" = 4 ]] && ORIENTATION=rotate
  [[ "$MO" = 3 ]] && flipy=1
  [[ "$MO" = 3 ]] && flipx=1
  [[ "$MO" = 1 ]] && ORIENTATION=vertical-right
  [[ "$MO" = 2 ]] && ORIENTATION=vertical-left
  [[ "$MO" = 3 ]] && ORIENTATION=flipped
  # Reset values
  set_mame_config_value "rol" 0
  set_mame_config_value "ror" 0
  set_mame_config_value "autoror" 0
  set_mame_config_value "autorol" 0


  # Monitor Orientation
  # Don't enable the rotate drm flag, it breaks panel_orientation
  case "$ORIENTATION" in
    horizontal) kernel_remove_param fbcon
                echo -n 0 | sudo tee /sys/class/graphics/fbcon/rotate
                kernel_remove_video_subparam rotate
                kernel_remove_video_subparam panel_orientation
                unrotate_am_layout
                ;;
    vertical-left) set_mame_config_value "rol" 1
              kernel_edit_param fbcon "rotate:3"
              echo -n 3 | sudo tee /sys/class/graphics/fbcon/rotate
              #kernel_edit_video_subparam rotate 270
              kernel_edit_video_subparam panel_orientation left_side_up
              rotate_am_layout left
              ;;
    vertical-right) set_mame_config_value "ror" 1
              kernel_edit_param fbcon "rotate:1"
              echo -n 1 | sudo tee /sys/class/graphics/fbcon/rotate
              #kernel_edit_video_subparam rotate 90
              kernel_edit_video_subparam panel_orientation right_side_up
              rotate_am_layout right
              ;;
    flipped) kernel_edit_param fbcon "rotate:2"
             echo -n 2 | sudo tee /sys/class/graphics/fbcon/rotate
             #kernel_edit_video_subparam rotate 180
             kernel_edit_video_subparam panel_orientation upside_down
             rotate_am_layout flip
             ;;
    rotate) set_mame_config_value "ror" 0
            set_mame_config_value "rol" 0
            set_mame_config_value "autoror" 1
            set_mame_config_value "autorol" 0
            kernel_remove_param fbcon
            kernel_remove_video_subparam rotate
            kernel_remove_video_subparam panel_orientation
            unrotate_am_layout
            ;;
  esac
  set_config_value "$GA_CONF" orientation "$ORIENTATION"
  set_mame_config_value flipx "$flipx"
  set_mame_config_value flipy "$flipy"
  return 0
}


worker_select_monitor_aspect () {
  local default=no
  # Aspect
  ask_option $default "Monitor Aspect" "What aspect ratio is your monitor?" required \
    "1" "4:3 (standard CRT)" \
    "2" "5:4" \
    "3" "16:9 (HDTV)" \
    "4" "16:10" \
    "5" "Auto" \
    "6" "Return to main" \
    || return 1

  MA=$ANSWER_OPTION
  [[ "$MA" = 1 ]] && ASPECT=" 4:3"
  [[ "$MA" = 2 ]] && ASPECT=" 5:4"
  [[ "$MA" = 3 ]] && ASPECT="16:9"
  [[ "$MA" = 4 ]] && ASPECT="16:10"
  [[ "$MA" = 5 ]] && ASPECT="auto"
  [[ "$MA" = 6 ]] && return 0

  inform "Setup MAME for $ASPECT monitor..."

  set_mame_config_value "aspect" "$ASPECT"
  set_config_value "$GA_CONF" aspect "$ASPECT"

  return 0
}


worker_select_force_aspect_vertical () {
  local default=no

  # Aspect
  ask_option $default "Monitor Aspect" "What aspect do you like for vertical games on horizontal monitor?" required \
    "1" "Normal (default)" \
    "2" "Wider" \
    "3" "Square" \
    "4" "Stretch to Full Screen" \
    "5" "Return to Main" \
    || return 1

  MA=$ANSWER_OPTION
  [[ "$MA" = 1 ]] && ASPECT="4:3"
  [[ "$MA" = 2 ]] && ASPECT="7:5"
  [[ "$MA" = 3 ]] && ASPECT="3:3"
  [[ "$MA" = 4 ]] && ASPECT="3:4"
  [[ "$MA" = 5 ]] && return 0

  inform "Setup MAME for $ASPECT monitor..."

  set_config_value  "$GA_CONF" aspect "$ASPECT"

  # MAME INI
  # Subs: monitor_aspect is not even a valid mame option
  #sed -e 's/^monitor_aspect\s.*/#BLANK/g' /home/arcade/ini/vertical.ini | grep -v "^#BLANK$" > /home/arcade/ini/vertical.ini # SBS: Should this remove monitor_aspect ??? Anyway it's unstable
  set_mame_config_value "MAME_DIR"/ini/vertical.ini aspect "$ASPECT"

  return 0
}


worker_select_monitor_crtlcd ()
{
  local default=no
  lcd=$(grep -r 'monitor' "$MAME_INI" |cut -d" " -f3)
  lcdd=$(echo "$lcd" | cut -d "_" -f 1)

  if [[ "$lcd" != "lcd" ]] && [[ "$lcdd" != "vesa" ]] ; then
    notify "You can change the GLSL CRT-EMU only if your display is a LCD or VESA"
    return 0
  fi

  Nowglsl=$(grep -r 'gl_glsl[[:space:]]' mame.ini|cut -d" " -f3)
  if [ "$Nowglsl" != "1" ]; then
    Nowglsl=Disable
  else
    Nowglsl=Enable
  fi

  while true ; do
  # Aspect #monitor  lcd crt-emu
  ask_option $default "GLSL Monitor CRT Emulator" "Configure your GLSL Emulator" required \
    1 "Gamma of simulated CRT" \
    2 "Gamma of display monitor" \
    3 "Overscan" \
    4 "Aspect ratio" \
    5 "Simulated distance from viewer to monitor" \
    6 "Radius of curvature" \
    7 "Tilt angle in radians" \
    8 "Size of curved corners" \
    9 "Border smoothness" \
    10 "Enable/Disable GLSL CRT Emulator - GLSL is $Nowglsl -" \
    11 "Set all Default" \
    12 "Return to Main" \
    || return

    case $ANSWER_OPTION in
      "1")
        valor=$(grep -r 'CRTgamma =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Gamma of simulated CRT Default = 2.4" "$valor" || return 0
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/CRTgamma =.*/CRTgamma = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=2 ;;
      "2")
        valor=$(grep -r 'monitorgamma =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Gamma of display monitor Default = 2.2" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/monitorgamma =.*/monitorgamma = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=3 ;;
      "3")
        valor=$(grep -r 'overscan =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d"(" -f2|cut -d" " -f2|cut -d";" -f1|cut -d")" -f1)
        ask_string "Overscan Default = 1.01,1.01" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/overscan =.*/overscan = vec2($scrt);/g" /home/arcade/CRT/CRT-geom.vsh  && default=4 ;;
      "4")
        valor=$(grep -r 'aspect =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d"(" -f2|cut -d" " -f2|cut -d";" -f1|cut -d")" -f1)
        ask_string "Aspect ratio Default = 1.0,0.75" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/aspect =.*/aspect = vec2($scrt);/g" /home/arcade/CRT/CRT-geom.vsh && default=5 ;;
      "5")
        valor=$(grep -r ' d =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Simulated distance Default = 2.0" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/d =.*/d = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=6 ;;
      "6")
        valor=$(grep -r 'R =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Radius of curvature Default = 2.0" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/R =.*/R = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=7 ;;
      "7")
        valor=$(grep -r 'vec2 angle = vec2' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d"(" -f2|cut -d" " -f2|cut -d";" -f1|cut -d")" -f1)
        ask_string "Tilt angle in radians Default = 0.0,-0.15" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/angle =.*/angle = vec2($scrt);/g" /home/arcade/CRT/CRT-geom.vsh && default=8 ;;
      "8")
        valor=$(grep -r 'cornersize =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Size of curved corners Default = 0.03" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/cornersize =.*/cornersize = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=9 ;;
      "9")
        valor=$(grep -r 'cornersmooth =' /home/arcade/CRT/CRT-geom.vsh |cut -d"=" -f2|cut -d" " -f2|cut -d";" -f1)
        ask_string "Border smoothness Default = 1000.0" "$valor" || return 1
        scrt=${ANSWER_STRING// /}
        sudo sed -ri "s/cornersmooth =.*/cornersmooth = $scrt;/g" /home/arcade/CRT/CRT-geom.vsh && default=10 ;;
      "10")
        activado=$(grep -r 'gl_glsl[[:space:]]' "$MAME_INI" | cut -d" " -f3)
        if [ "$activado" != "1" ]; then
          mensa=Disable
          valor=1
        else
          mensa=Enable
          valor=0
        fi
        ask_yesno "GLSL CRT Emulator is $mensa\nDo you want change?" || nada=1
        if [[ "$nada" != "1" ]] ; then
          sudo sed -ri "s/gl_glsl[[:space:]].*/gl_glsl			  $valor/g" /home/arcade/mame.ini
        fi
        nada=""
        default=11 ;;
      "11")
        cp /home/arcade/CRT/CRT-geom.vsh.Default /home/arcade/CRT/CRT-geom.vsh	&& default=12 ;;
      "12") return 0 ;;
      *)	notify "Bad option $ANSWER_OPTION given";;
      esac
  done
  return 0
}


worker_select_multisync_monitor() {
  local default=no
  local MONITOR=generic
  ask_option $default "Monitor Type" "What kind of monitor are you using?" required \
    "1" "D9200 15.25-31.5KHz" \
    "2" "D9800 15.25-38KHz" \
    "3" "M3192 15.25-31.5KHz" \
    "4" "M2929 30KHz-50KHz" || return 1
  [ "$ANSWER_OPTION" = 1 ] && MONITOR=d9200
  [ "$ANSWER_OPTION" = 2 ] && MONITOR=d9800
  [ "$ANSWER_OPTION" = 3 ] && MONITOR=m3192
  [ "$ANSWER_OPTION" = 4 ] && MONITOR=m2929

  set_monitor $MONITOR && return 0
  return 1
}


worker_video_boot_options() {
  local default=no
  Nowsplash=$(grep -i splash $(find_syslinux_file) )
  Nowtime=$(grep 'timeout' $(find_syslinux_file) | awk '{print $2}')
  Hsplash=$(grep 'Hsplash=' "$CONFIG_FILE" | awk -F = '{print $2}')

  if [ "$Nowsplash" != "" ]; then
    Nowsplash="Enable"
  else
    Nowsplash="Disable"
  fi

  if [ "$Hsplash" == "" ]; then
    Hsplash="Horizontal"
    sed -ri "s/Hsplash=.*//g" "$CONFIG_FILE"
    sed -ri '/^$/d' "$CONFIG_FILE"
    echo "Hsplash=Horizontal" >> "$CONFIG_FILE"
  fi

  while true ; do
    ask_option $default "Video Boot Options" "Setup video" required \
      1 "Video Card Output KHz Auto" \
      2 "Video Card Output Khz List" \
      3 "Syslinux's boot timeout - Timeout is $Nowtime " \
      4 "Splash   Enable/Disable - Splash is $Nowsplash " \
      5 "Splash   Orientation    - Splash is $Hsplash " \
      6 "Return to Main" \
      || return 1

      case $ANSWER_OPTION in
        "1") execute worker vga_mode && default=2 ;;
        "2") execute worker video_syslinux && default=3 ;;
        "3") ask_string "Change Boot Time\n1 second = 10 (Xsec x 10) " "0" || return
          TIME=${ANSWER_STRING// /}
          if [ "$TIME" != "0" ]; then
            sudo sed -ri "s/^[#]*UI.*/UI vesamenu.c32/" $(find_syslinux_file)
            sudo sed -ri "s/^timeout.*/timeout $TIME/" $(find_syslinux_file)
          else
            sudo sed -ri "s/^[#]*UI.*/#UI vesamenu.c32/" $(find_syslinux_file)
            sudo sed -ri "s/^timeout.*/timeout $TIME/" $(find_syslinux_file)
          fi
          default=4 ;;
        "4")
          SPLASH=$(grep -i splash $(find_syslinux_file) )
          if [[ -z "$SPLASH" ]]; then
            SPLASH="quiet rd.udev.log-priority=3 splash"
            FIND="quiet"
            STATUS="Disable"
          else
            SPLASH="quiet"
            FIND="quiet rd.udev.log-priority=3 splash"
            STATUS="Enable"
          fi
          ask_yesno "Splash is $STATUS\nDo you want to change it?" || nada=1
          if [ "$nada" != "1" ]; then
            sudo sed -ri "s/$FIND/$SPLASH/" $(find_syslinux_file);
          fi
          nada="" && default=6 ;;
        "5") while true ; do
            ask_option $default "Splash   Orientation" "Setup Boot Splash" required \
              1 "Horizontal" \
              2 "Vertical Right" \
              3 "Vertical Left" \
              4 "Return to Main" \
              || return

            case $ANSWER_OPTION in
              "1")
                OR="Horizontal"
                cp /usr/share/plymouth/lh.png /usr/share/plymouth/arch-logo.png && default=4 ;;
              "2")
                OR="Vertical Right"
                cp /usr/share/plymouth/lvr.png /usr/share/plymouth/arch-logo.png && default=4 ;;
              "3")
                OR="Vertical Left"
                cp /usr/share/plymouth/lvl.png /usr/share/plymouth/arch-logo.png && default=4 ;;
              "4") return 0 ;;
            esac
            sed -ri "s/Hsplash=.*/Hsplash=$OR/g" "$CONFIG_FILE"
            inform "Regenerating Splash...\n"
            mkinitcpio -p linux-15khz > /dev/tty12 2>&1
          done
          none=""
          default=6 ;;
        "6") return 0 ;;
        *) notify "Bad option $ANSWER_OPTION given" ;;
      esac
  done
  return 0
}


worker_vga_mode() {
  CGA=""
  INTERFACES=""
  mount -t ext2 -a
  GRUB_FILE=$(find_syslinux_file)

  if [[ ! -f "$(find_syslinux_file)" ]] ; then
    notify "You can only change the interface on an installed system. Not in the LiveCD"
    return 1
  fi

  local default=no
  PCIIDORIG=$(lspci -v | grep VGA | head -1 | awk "{'printf("%s",$1)'}") # SBS: Doesn"t match "Display"
  PCIID=$(lspci -v | grep VGA | head -1 | sed -e 's/\./:/' | awk "{'printf("PCI:%s",$1)'}") # SBS: Doesn't match "Display"
  ALLDEVICES=$(find /sys/devices/pci0000\:00/ -iname card\*-\*)

  for line in $ALLDEVICES
  do
    WORDS=$(echo "$line" | sed -e 's/\// /g')
    START=0
    for dev in $WORDS ; do
      IS_DEV=$(echo "$dev" | grep card0-)
      if [[ "$START" = "1" ]] && [[ "$IS_DEV" != "" ]] ; then
        THISDEV=$(echo "$dev" | sed -e 's/card[0-9]-//g')
        if [[ -z "$INTERFACES" ]] ; then
          INTERFACES="${THISDEV} ${PCIID}"
        else
          INTERFACES="${INTERFACES} ${THISDEV} ${PCIID}"
        fi
      fi
      if [[ "$dev" = "0000:$PCIIDORIG" ]] ; then
        START=1
      fi
    done
  done

  while true ; do
    ask_option $default "Video Output KHz Mode" "Pick Video interface to change KHz" required $INTERFACES \
      "Done" "-" \
      || return 1

    [[ "$ANSWER_OPTION" = "Done" ]] && return 0

    default=Done

    #NAME=$(echo $ANSWER_OPTION | awk -F _ '{printf("%s", $1)}')
    NAME=$ANSWER_OPTION
    #VIDEO=$(cat $GRUB_FILE | grep "kernel " | head -1)
    VIDEO=$(grep "append " "$GRUB_FILE" | head -1)
    CGA="no"
    KLINE=""
    VLINE=""

    for karg in $VIDEO ; do
      IS_VIDEO=$(echo "$karg" | grep "video=")
      if [[ ! -z "$IS_VIDEO" ]] ; then
        IS_THIS=$(echo "$karg" | grep $NAME)
        if [[ ! -z "$IS_THIS" ]] ; then
          CGA="yes"
        else
          if [[ -z "$VLINE" ]] ; then
            VLINE=$karg
          else
            VLINE="$VLINE $karg"
        fi
      fi
    else
      if [[ -z "$KLINE" ]] ; then
        KLINE=$karg
      else
        KLINE="$KLINE $karg"
      fi
    fi
  done

  default2=no
  ask_option $default2 "Interface Type" "Pick output mode of interface" required \
    1 "CGA 15.75khz" \
    2 "NTSC TV (60 Hz only)" \
    3 "PAL TV (50 Hz only)" \
    4 "VGA 31.5khz" \
    5 "Disable Output" \
    6 "Cancel" \
    || return 1

  RES="640x480"
  case $ANSWER_OPTION in
    "1") CGA="yes" && RES="640x480" && default2=6 ;;
    "2") CGA="yes" && RES="720x480" && default2=6 ;;
    "3") CGA="yes" && RES="768x576" && default2=6 ;;
    "4") CGA="no" && default2=6 ;;
    "5") CGA="yes" && RES="disable" && default2=6 ;;
    "6") CGA="" && default2=6 ;;
    *) notify "Bad option $ANSWER_OPTION given" ;;
  esac

  if [ "$CGA" != "" ]; then
    if [ "$VLINE" = "" ]; then
      NEWLINE=$KLINE
    else
      NEWLINE="$KLINE $VLINE"
    fi
    if [ "$CGA" = "yes" ]; then
      if [ "$RES" = "disable" ]; then
        NEWLINE="$NEWLINE video=${NAME}:d"
      else
        NEWLINE="$NEWLINE video=${NAME}:${RES}ec"
      fi
    fi

    ask_yesno "Use new kernel command line: '$NEWLINE' ?\n" || return 1

    IMAGE=$(grep "kernel " "$GRUB_FILE" | awk "{'print $2'}" | head -1)
    sed -e 's/.*kernel .*/_REPLACE_ME_/g' "$GRUB_FILE" > /tmp/grub.tmp
    sed -e "s|_REPLACE_ME_|$NEWLINE|g" /tmp/grub.tmp > /tmp/grub.conf
    rm -f /tmp/grub.tmp

    ask_yesno "Overwrite grub.conf with new one?\n" || return 1
    mv /tmp/grub.conf "$GRUB_FILE"
  fi
  done

  notify "You will now need to pick your monitor type again."
  select_monitor_menu # SBS: Can't work,it's not the worker
  notify "You will need to reboot for the changes to take effect."
  return 0
}


worker_video_syslinux()
{
  MODO=""
  mount -t ext2 -a
  GRUB_FILE=$(find_syslinux_file)
  if [[ ! -f "$(find_syslinux_file)" ]] ; then
    notify "You can only change the video boot-syslinux on an installed system. Not in the LiveCD."
    return 0
  fi

  local default=no
  while  true ; do # SBS: no no no do it depending on the video cards connectors
    ask_option $default "Change video boot-syslinux" "Select the connector you want to use for video output on boot, as well as the frequency (kHz)" required \
      1 "[DVI-1 15khz] " \
      2 "[VGA-1 15khz] " \
      3 "[DVI-2 15khz] " \
      4 "[VGA-2 15khz] " \
      5 "[NTSC DVI-1 15khz (60 Hz only)] " \
      6 "[NTSC VGA-1 15khz (60 Hz only)] " \
      7 "[PAL DVI-1 15khz (50 Hz only)] " \
      8 "[PAL VGA-1 15khz (50 Hz only)] " \
      9 "[SVGA/VGA/LCD Monitor] " \
      10 "[DVI-1 15khz pci=nomsi (Use for buggy motherboards)] " \
      11 "[VGA-1 15khz pci=nomsi (Use for buggy motherboards)] " \
      12 "[DVI-1 25khz] " \
      13 "[VGA-1 25khz] " \
      14 "[DVI-2 25khz] " \
      15 "[VGA-2 25khz] " \
      16 "[DVI-1 31khz] " \
      17 "[VGA-1 31khz] " \
      18 "[DVI-2 31khz] " \
      19 "[VGA-2 31khz] " \
      20 "Return to Main" \
      || return 1

    case $ANSWER_OPTION in # SBS: no no no do it depending on the video cards connectors
      "1")  MODO="DVI-I-1:640x480ec" && default=20 ;;
      "2")  MODO="VGA-1:640x480ec"  && default=20 ;;
      "3")  MODO="DVI-I-2:640x480ec" && default=20 ;;
      "4")  MODO="VGA-2:640x480ec" && default=20 ;;
      "5")  MODO="DVI-I-1:720x480ec" && default=20 ;;
      "6")  MODO="VGA-1:720x480ec" && default=20 ;;
      "7")  MODO="DVI-I-1:768x576ec" && default=20 ;;
      "8")  MODO="VGA-1:768x576ec" && default=20 ;;
      "9")  MODO="" && default=20 ;;
      "10") MODO="DVI-I-1:640x480ec pci=nomsi" && default=20 ;;
      "11") MODO="VGA-1:640x480ec pci=nomsi" && default=20 ;;
      "12") MODO="DVI-I-1:512x384ez" && default=20 ;;
      "13") MODO="VGA-1:512x384ez"  && default=20 ;;
      "14") MODO="DVI-I-2:512x384ez" && default=20 ;;
      "15") MODO="VGA-2:512x384ez" && default=20 ;;
      "16") MODO="DVI-I-1:640x480ey" && default=20 ;;
      "17") MODO="VGA-1:640x480ey"  && default=20 ;;
      "18") MODO="DVI-I-2:640x480ey" && default=20 ;;
      "19") MODO="VGA-2:640x480ey" && default=20 ;;
      "20")	return 0 ;;
      *)	notify "Bad option $ANSWER_OPTION given";;
    esac

    if [[ "$ANSWER_OPTION" != "10" ]] && [[ "$ANSWER_OPTION" != "11" ]] ; then
      sudo sed -ri "s/pci=.*//" $(find_syslinux_file)
    fi

    VIDEO=$(grep "video=" $(find_syslinux_file))
    if [[ -z "$VIDEO" ]] ; then
      sudo sed -ri "s/vga=.*/vga=0x311 video=/" $(find_syslinux_file)
    fi

    sudo sed -ri "s/video=.*/video=$MODO/" $(find_syslinux_file)

  done
  return 0;
}


worker_video_custom() {

  #while [ ! -f "/etc/X11/xorg.conf" ]; do
  if [ ! -f "/etc/X11/xorg.conf" ]; then
    notify "Please Select Monitor type"
    worker_select_monitor_menu
  fi
  #done

  local default=no
  while [ 0 = 0 ]; do
    ask_option $default "Custom Video Mode & Presets" "Video Mode & Presets" required \
      1 "Xorg/Desktop" \
      2 "MAME/Switchres Presets" \
      3 "Video Resolution Boot " \
      4 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") execute worker xorg_custom && default=2 ;;
      "2") execute worker crtrange_presets && default=3 ;;
      "3") execute worker boot_custom && default=4 ;;
      "4") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}


worker_xorg_custom() {
  local default=no
  ask_option $default "Choose Video Mode" "" required \
    1 "Manual Video Mode" \
    2 "List Video Mode" || return

  if [ $ANSWER_OPTION = "1" ]; then
    ask_string "Example Custom Video Mode (648 480 60)" "" || return 1
    MODELI=${ANSWER_STRING}
    inform "$MODELI"
    sleep 1
    MONITOR=$(cat /home/arcade/switchres.conf | grep monitor | sed -e 's/monitor=//g')
    inform $MONITOR
    sleep 1
    #switchres --calc $MODELI --monitor $MONITOR >>/etc/X11/xorg.conf
    #/usr/local/bin/xorg.sh $MONITOR > /etc/X11/xorg.conf
    return 0
  else
    local default=no
    ask_option $default "Custom Video Mode" "" required \
      1 "328x240@60" \
      2 "648x480@60" \
      3 "728x480@59" \
      4 "768x576@60" \
      5 "808x600@50" \
      6 "1032@768@40" || return
    case $ANSWER_OPTION in
      "1") VMODE="328 240 60" ;;
      "2") VMODE="648 480 60" ;;
      "3") VMODE="728 480 59" ;;
      "4") VMODE="768 576 60" ;;
      "5") VMODE="808 600 60" ;;
      "6") VMODE="1032 768 60" ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac

    MONITOR=$(cat /home/arcade/switchres.conf | grep monitor | sed -e 's/monitor=//g')
    VSMODE=$(switchres --calc $VMODE --monitor h9110 | grep "#" | sed -e 's/#  //')
    VMODE=$(echo $VMODE | sed -e '1s/ /X/1' | sed -e '1s/ /@/1')
    notify "Video Mode Select $VMODE New Video Mode $VSMODE"

    return 0
  fi

}


worker_crtrange_presets() {
  local default=no
  while [ 0 = 0 ]; do
    ask_option $default "Presets MAME/Switchres" "" required \
      0 "crt_range0" \
      1 "crt_range1" \
      2 "crt_range2" \
      3 "crt_range3" \
      4 "crt_range4" \
      5 "crt_range5" \
      6 "crt_range6" \
      7 "crt_range7" \
      8 "crt_range8" \
      9 "crt_range9" \
      10 "Return to Main" || return
    case $ANSWER_OPTION in
      [0-9])
        RANGO="crt_range$ANSWER_OPTION"
        #RVALOR=$(grep -ri $RANGO /home/arcade/.mame/mame.ini|awk -F $RANGO '{print $2}')
        RVALOR=$(echo$(awk '/'$RANGO'/ {$1=""; print }' /home/arcade/.mame/mame.ini))
        ask_string "Parameters:\n\
								\nHfreqMin-HfreqMax, VfreqMin-VfreqMax, HFrontPorch, HSyncPulse, HBackPorch, VfrontPorch, VSyncPulse, VBackPorch, HSyncPol, VSyncPol, ProgressiveLinesMin, ProgressiveLinesMax, InterlacedLinesMin, InterlacedLinesMax\n
								\nExample:\n
								crt_range0  15625.00-15625.00, 50.00-50.00, 1.500, 4.700, 5.800, 0.064, 0.160, 1.056, 0, 0, 192, 288, 448, 576\n\n
								\nYour Values:\n
								$RVALOR" "$RVALOR" "Select your Present for $RANGO:"
        crtr=$ANSWER_STRING
        if [ -z "$ANSWER_STRING" ]; then
          notify "Defaults Value Auto\n"
          crtr="auto"
        fi
        sudo sed -ri "s/^$RANGO.*/$RANGO $crtr/" /home/arcade/.mame/mame.ini
        sudo sed -ri "s/^$RANGO.*/$RANGO $crtr/" /home/arcade/switchres.conf
        default=$(expr $ANSWER_OPTION + 1)
        ;;
      10) return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}


worker_boot_custom() {
  local default=no
  ask_option $default "Custom Video Boot" "" required \
    1 "320x240@60-15Khz" \
    2 "640x480@60-15Khz" \
    3 "720x480@59-15Khz" \
    4 "760x576@60-15Khz" \
    5 "800x600@50-15Khz" \
    6 "1032@768@40-15Khz" \
    7 "320x240@60-25Khz" \
    8 "512x384@58-25Khz" \
    9 "800x600@60-25Khz" \
    10 "1024x768@50-25Khz" \
    11 "640x480@60-31Khz" || return
  case $ANSWER_OPTION in
    "1") VBMODE="328 240 60" ;;
    "2") VBMODE="648 480 60" ;;
    "3") VBMODE="728 480 59" ;;
    "4") VBMODE="768 576 60" ;;
    "5") VBMODE="808 600 60" ;;
    "6") VBMODE="1032 768 60" ;;
    *) notify "Bad option $ANSWER_OPTION given" ;;
  esac

  notify "Video Mode Select $VBMODE"

  return 0
}


worker_fix_video() {

  local default=no
  while [ 0 = 0 ]; do
    ask_option $default "Fix Video Problems" "Fix Video Problems" required \
      1 "Fast Speed in Games" \
      2 "Force Xorg.conf" \
      3 "Force MinDotClock" \
      4 "Fix Tearing " \
      5 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") execute worker fast_speed && default=2 ;;
      "2") execute worker force_xorg && default=3 ;;
      "3") execute worker mindotclock && default=4 ;;
      "4")
        ask_yesno "\nDo you want change to Enable this Options?\n\n\n" || op=1
        if [ "$op" != "1" ]; then
          sed -ri "s/^[#]*Option ShadowPrimary on/Option ShadowPrimary on/" /etc/X11/xorg.conf
        else
          sed -ri "s/^[#]*Option ShadowPrimary on/#Option ShadowPrimary on/" /etc/X11/xorg.conf
        fi
        op=""
        default=5
        ;;
      "5") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done

  return 0
}


worker_fast_speed() {
  notify "Regenerating Kernel\nPlease Reboot Arcade PC "
  mkinitcpio -p linux >/dev/tty12 2>&1
  return 0
  return
}


worker_force_xorg() {
  sudo X -configure >/dev/tty12 2>&1
  cp /root/xorg.conf.new /etc/X11/xorg.conf >/dev/tty12 2>&1
  notify "\nNow you have a new xorg.conf from local hardware\n"
  return 0
}


worker_mindotclock() {
  MDOT=$(grep "^[#]*dotclock_min" /home/arcade/mame.ini | awk '{print $2}')
  ask_string "Change MinDotClock\nDefault=0 Recommend=8" "$MDOT" || return 1
  MDOTT=${ANSWER_STRING// /}
  set_mame_config_value dotclock_min "$MDOTT"
  return 0
}


worker_kernel_video_boot() {
  local monitor=
  local hfreq=
  local video=
  local connector=
  local resol=
  local enabled=
  local resolutions_list=()
  local new_resol=

  # Get the current monitor type
  monitor=$(get_config_value "$GA_CONF" monitor)
  # Get the kernel video connector
  # The code expects a single value returned, will fail otherwise
  video=$(kernel_get_cmdline_param video)
  connector=$(echo $video | cut -d ":" -f 1)
  # Get connector resolution
  resol=$(echo $video | grep -oE "[0-9]{3,4}x[0-9]{3,4}[iS]{0,2}")
  # Is the connector forced ?
  echo "$video" | cut -d "=" -f 2 | grep -q "e" && enabled=e

  # TODO: reject for NVIDIA and INTEL resolution change for now
  # Use the connector, find the card, find its kernel module

  # If the connector is empty, we can't do anything more
  assert_info "Couldn't detect the connector, aborting" test -n "$connector" || return 1

  # Now we've got it all, time to check what kind of monitor we have
  source /opt/gatools/video/monitor.sh
  source /opt/gatools/video/video.sh

  # Is the monitor worth changing resolution ?
  if ! hfreq=$(monitor_type "$monitor") ; then
    # No it's not, so just inform and quit
    inform "The configured monitor doesn't need a change in resolution." ; sleep 5
    return 1
  fi

  # Can we use low dotclock resolutions ?
#  if ! connector_supports_low_dot_clock "$connector" ; then
#    # Nope. Explain user that ATI/AMD cards rock, kiss good night
#    inform "The video card doesn't support low pixel clocks. You can't change the resolution." ; sleep 5
#    return 1
#  fi

  if [[ $hfreq == 15k ]] ; then
    resolutions_list+=("320x240S" "320x240@60 progressive")
    resolutions_list+=("384x288S" "384x288@50 progressive")
    resolutions_list+=("640x240S" "640x240@60 progressive")
    resolutions_list+=("640x480iS" "640x480@60 interlaced")
    resolutions_list+=("720x480iS" "720x480@60 interlaced")
    resolutions_list+=("758x576iS" "758x576@50 interlaced")
    resolutions_list+=("1280x480iS" "1280x480@60 interlaced")
  elif [[ $hfreq == 25k ]] ; then
    resolutions_list+=("512x384S" "512x384@59 progressive")
    resolutions_list+=("800x600iS" "800x600@60 progressive")
    resolutions_list+=("1024x768iS" "1024x768@50 interlaced")
  fi

  # Allow custom resolutions through EDID
  resolutions_list+=("custom" "Set your own resolution")

  # Make the cutest menu ever
  if ! ask_option "${resol:=no}" \
    "Select a boot resolution" \
    "You will need to reboot to take effect" \
    optional \
    "${resolutions_list[@]}" ; then
    return 1
  fi

  # Now we're ready to set the boot resolution
  new_resol="$ANSWER_OPTION"
  # Check the answer is valid
  if [[ $new_resol != "custom" ]] ; then
    echo "$new_resol" | grep -qE "^[0-9]{3,4}x[0-9]{3}" || return 1 # No it's not, user cancelled
    sudo sed -Ei "s/video=[[:alnum:][:punct:]]+/video=${connector}:${new_resol}${enabled}/" $(find_syslinux_file)
    return $?
  fi

  # At this point, we ARE in custom mode
  execute worker custom_video_mode
}

rotate_am_layout() {
  orientation="$1"
  f="$GA_HOME/shared/frontends/attract/attract.cfg"
  if grep -q screen_rotation "$f" ; then
    set_mame_config_value "$f" screen_rotation $orientation
  else
    log_info
    sed -i "/^[[:space:]]window_mode         /a \\\tscreen_rotation      $orientation" "$f"
  fi
}

unrotate_am_layout() {
  val="none"
  f="$GA_HOME/shared/frontends/attract/attract.cfg"

  if grep -q screen_rotation "$f" ; then
    set_mame_config_value "$f" screen_rotation $val
  else
    sed -i "/^[[:space:]]window_mode         /a \\\tscreen_rotation      $val" "$f"
  fi
}


check_compatible_kms_emulators() {
  # First: make sure we have at least a KMS compatible emulator
  local found_kms_emu=0
  declare -A kms_compatible_emus
  kms_compatible_emus["groovymame"]="0.238-1"
  #kms_compatible_emus["retroarch"]="1.9.14-2"

  for e in "${!kms_compatible_emus[@]}" ; do
    emu_version=$(pacman -Q "$e" | cut -d ' ' -f 2)
    version_diff=$(vercmp "$emu_version" "${kms_compatible_emus[$e]}")
    if [[ "$version_diff" -ge "0" ]] ; then
      found_kms_emu=1
      break;
    fi
  done

  # Warn and leave if no compatible emulator was found
  if [[ $found_kms_emu == 0 ]] ; then
    msg="You need at least one of the following emulators with the required version installed:"
    for e in "${!kms_compatible_emus[@]}" ; do
      emu_version="$(pacman -Q "$e")"
      msg+="$(echo -e "\n$e ${kms_compatible_emus[$e]}")"
    done
    notify "$msg"
    return 1
  fi
  return 0
}


worker_select_and_install_frontend() {
  local curr_video="$1"
  curr_video=${curr_video:-X}
  inform "Building the frontends list for $curr_video..."
  fe_list=()
  declare -A fe_list_no_index
  idx=1
  fe_available=0

  if ! sudo pacman -Syq ; then
    inform "Couldn't update the pacman database, aborting" ; sleep 2
    return 1
  fi

  for fe in $(grep -E -o "^frontend\.[[:alnum:]-]*\." "$GA_VIDEO_DATA" | sort | uniq | cut -d '.' -f 2) ; do
    # Check if we can add either or both of stable + -git version
    # Skip unavailable
    if pacman -Si "$fe" &>/dev/null && check_frontend_video_backend_available "$curr_video" "$fe" ; then
      fe_available=1
      fe_list+=("$idx" "$fe")
      fe_list_no_index[$idx]="$fe"
      idx=$((idx + 1))
    fi
    if pacman -Si "$fe"-git &>/dev/null && check_frontend_video_backend_available "$curr_video" "$fe" ; then
      fe_available=1
      fe_list+=("$idx" "$fe"-git)
      fe_list_no_index[$idx]="${fe}-git"
      idx=$((idx + 1))
    fi
  done

  if [[ $fe_available == 0 ]] ; then
    inform "Couldn't find any emulator compatible with $curr_video" ; sleep 2
    return 1
  fi

  #Add lxde, despite it can't run on KMS, galauncher will manage that
  fe_list+=("$idx" "lxde")
  fe_list_no_index[$idx]="lxde"

  ask_option 1 "Select a frontend" '' required "${fe_list[@]}"
  ret_code=$?
  selected_fe="${fe_list_no_index[${ANSWER_OPTION}]}"
  if [[ -z "$selected_fe" && $ret_code == 0 ]] ; then
    notify "Couldn't set your FE to $selected_fe"
    return 1
  # User chose cancel
  elif [[ -z "$selected_fe" && $ret_code == 1 ]] ; then
    return 1
  fi

  # lxde is a special case as it's not a package but a group. Let's just make it simple
  if [[ "$selected_fe" == lxde ]] ; then
    true
  # Install the FE if required
  elif ! pacman -Qi "$selected_fe" 2>/dev/null | head -1 |cut -d ':' -f 2 | tr -d ' ' | grep -q "^${selected_fe}$"; then
    ask_yesno "$selected_fe is missing on your system. Install it ?" || return 1
    if ! pacman -Sy --needed --noconfirm "$selected_fe" $opt_pkgs ; then
      inform "Failed installing $selected_fe, aborting"
      sleep 2
      return 1
    fi
  fi
  if ! set_config_value "$GA_CONF" frontend "$selected_fe" ; then
    notify "Couldn't set the frontend, you may not reach it"
    return 1
  fi
  inform "Successfully set your new frontend to $selected_fe" ; sleep 2
  log_info "Switched FE to $selected_fe on $curr_video"
  return 0
}


check_frontend_video_backend_available() {
  local video_backend="$1"
  local fe="${2%-git}"
  local fe_caps=$(get_config_value "$GA_VIDEO_DATA" "frontend.${fe}.videobackends")
  list_has_value "$fe_caps" "$video_backend" &>/dev/null
  return $?
}


check_and_ask_compatible_frontend() {
  local video_backend="$1"
  local curr_fe=$(get_config_value $GA_CONF frontend)
  local fe_caps=$(get_config_value "$GA_VIDEO_DATA" "frontend.${curr_fe}.videobackends")
  if ! list_has_value "$fe_caps" "$video_backend" ; then
    notify "Your current frontend doesn't support $video_backend. Please select a new one"
    worker_select_and_install_frontend "$video_backend"
    return $?
  fi
  inform "$curr_fe is supported on $video_backend. You may change it in gasetup as usual" ;sleep 2
  return 0
}

worker_select_video_backend() {
  ! check_compatible_kms_emulators && return 1

  while true ; do
    ask_option $default "Select a Video backend" "Current video backend: $curr_video" required \
      1 "Xorg" \
      2 "KMS" \
      3 "Return to Main" || return

    case $ANSWER_OPTION in
      "1") check_and_ask_compatible_frontend "X" && \
           set_config_value "$GA_CONF" "video.backend" X && \
           inform "Video backend set to X" || \
           inform "Failed setting the video backend to X" ; sleep 2
           return 0
           ;;
      "2") check_and_ask_compatible_frontend "KMS" && \
           set_config_value "$GA_CONF" "video.backend" KMS && \
           inform "Video backend set to KMS" || \
           inform "Failed setting the video backend to KMS" ; sleep 2
           return 0
           ;;
      "3") return 0 ;;
      *) notify "Bad option $ANSWER_OPTION given" ;;
    esac
  done
}

worker_custom_video_mode() {
  ask_string "Input width" "$(get_config_value "$GA_CONF" custom.width)" || return 1
  w=$ANSWER_STRING
  ask_string "Input height" "$(get_config_value "$GA_CONF" custom.height)" ||return 1
  h=$ANSWER_STRING
  ask_string "Input refresh rate" "$(get_config_value "$GA_CONF" custom.refresh_rate)" ||return 1
  rr=$ANSWER_STRING

  # Do the checks
  re='^[0-9\.]+$'
  for v in $w $h $rr ; do
    if ! [[ $v =~ $re ]] ; then
      inform "Error: $v is not a number" ; sleep 2
      return 1
    fi
  done

  # Test the mode so the user doesn't go black screen on reboot
  notify "The new mode will be tested. Press ESC if you don't see the grid, ENTER or Q to quit"
  export GRID_TEXT="Testing the new resolution"
  switchres $w $h $rr -s -l grid
  unset GRID_TEXT
  ask_yesno "Have you seen the grid ?"
    if [[ $? != 0 ]] ; then
    lok_ko "User cancelled changing the boot resolution because he couldn't see the grid"
    return 1
  fi

  # Generate the EDID
  log_info "Generating a custom EDID file for boot resolution : ${w}x${h}@$rr"
  edid_file=/usr/lib/firmware/edid/custom_resolution.bin
  ( mkdir /tmp/edid && cd /tmp/edid && switchres $w $h $rr -e && sudo cp *.bin $edid_file )
  if [[ $? != 0 ]] ; then
    inform "Couldn't generate the EDID, aborting" ; sleep 2
    return 1
  fi
  rm -rf /tmp/edid
  ask_yesno "The generated modeline is : $(switchres $w $h $rr -c |cut -d '"' -f2). Continue ?"
  if [[ $? != 0 ]] ; then
    lok_ko "User cancelled changing the boot resolution"
    return 1
  fi

  # Keep trace of the mode
  set_config_value "$GA_CONF" custom.width "$w"
  set_config_value "$GA_CONF" custom.height "$h"
  set_config_value "$GA_CONF" custom.refresh_rate "$rr"

  # Add the EDID file to the initramfs + regenerate the initramfs if needed
  if ! grep -q "^FILES=.*$edid_file" /etc/mkinitcpio.conf ; then
    sudo sed -i -E "s+(FILES=.*)\)+\1 $edid_file)+" /etc/mkinitcpio.conf
  fi
  log_info "Regenerate the initramfs with the new EDID"
  sudo mkinitcpio -P

  # Now we need to work on the kernel command line
  # 1st : Get the connector

  # Make sure there is only 1 connector to set, otherwise we're dommed
  nr_video_conns=$(grep -oE "video=[A-Z-]+-[0-9]" /proc/cmdline | wc -l)
  if [[ $nr_video_conns > 1 ]] ; then
    inform "Looks like you have several CRT connectors, the automatic tool can't decide which is the expected one. Aborting" ; sleep 2
    log_ko "Kernel has several connectors configured, can't go further"
    return 1
  elif [[ $nr_video_conns < 1 ]] ; then
    inform "Couldn't determine the expected video connector. Aborting" ; sleep 2
    log_ko "Kernel has no connector configured, can't go further"
    return 1
  fi

  # Maybe the connector was set in ga.conf ?
  connector=$(get_config_value $GA_CONF connector)
  # Well, it's not, so extract it from the command line
  curr_video_param="$(grep -oE "video=[A-Z-]+-[0-9]+" /proc/cmdline)"
  [[ -z $connector ]] && connector=${curr_video_param#*=}
  # If it's still empty, then it should be set on drm.edid_firmware
  if [[ -z $connector ]]  ; then
    current_fw="$(grep -oE "drm.edid_firmware=[A-Z-]+-[0-9]+" /proc/cmdline)"
    if [[ -z $current_fw ]] ; then
      inform "Couldn't determine your monitor connector, aborting." ; sleep 2
      log_ko "Couldn't find the connector"
      return 1
    fi
    connector=${current_fw#*=}
  fi
  log_info "Resolution will change for connector : $connector"
  set_config_value $GA_CONF connector "$connector"

  # 2nd : clean the video= parameter
  # This removes the 15kHz specific values, it's harmless otherwise. We need to
  # do it, even if there was nothing set for 15kHz on video=
  curr_video_param="$(grep -E -o "video=[a-zA-Z0-9:,-_]+" /proc/cmdline)"
  new_video_param="$(rework_kernel_video "$curr_video_param")"
  # Edit the parameter in the syslinux.cfg
  syslinux_file=$(find_syslinux_file)
  sudo sed -iE "s%$curr_video_param%$new_video_param%" "$syslinux_file"

  # 3rd : add the custom edid for the connector
  sudo sed -Ei "/^append root=.*/ s%$curr_video_param%$new_video_param%" "$syslinux_file"
  short_edid="edid/$(basename $edid_file)"
  if cat /proc/cmdline | grep -q "drm.edid_firmware=${connector}" ; then
    sudo sed -Ei "/^append root=.*/ s%drm.edid_firmware=${connector}:edid\/[0-9a-z_]+.bin% drm.edid_firmware=${connector}:$short_edid%" "$syslinux_file"
  else
    sudo sed -Ei "/^append root=.*/ s%$% drm.edid_firmware=${connector}:$short_edid%" "$syslinux_file"
  fi
  log_ok "New resolution set!"
}

# $1 = video parameter to rework
rework_kernel_video() {
  param=${1#*=}
  #declare -A final_param
  final_param=()
  for p in $(echo $param | tr ',' '\n') ; do
    # Is it a switchres resolution ?
    v=
    if echo $p | grep -E -q "[0-9]+x[0-9]+.*S" ; then
      # Remove the forced resolution and the iaS flags
      v="$(echo $p | sed -E -e "s/[0-9]+x[0-9]+//" -e "s/[iaS]+//")"
    else
      v="$p"
    fi
    [[ ! -z $v ]] && final_param+=("$v")
  done
  # Now recompose the parameter
  echo video="$(IFS=, ; echo "${final_param[*]}")"
}

worker_adjust_geometry() {
  # Start the grid at 648x480 to avoid conflicts with default video mode
  inform "Calibrating geometry..."
  local final_crt_range="$(geometry 648 480 60 | grep "Final crt_range:")"
  # If the grid returned a final crt_range, update /etc/switchres.ini
  if [[ -z $final_crt_range ]] ; then
    inform "Looks like you've aborted" ; sleep 2
    return 1
  fi
  final_crt_range="$(echo $final_crt_range | sed "s/Final crt_range: //")"
  # monitor -> custom. If already custom, check that crt_ranges > 0 are auto
  # and inform user the current range will be overwritten
  #current_monitor="$(get_mame_config_value "$SWITCHRES_INI" monitor)"
  if [[ $current_monitor == custom ]] ; then
    ask_yesno "You're already using a custom monitor. OK to overwrite your crt_range0 ?" || return 1
  fi
  # Saved, done. Can't sudo the includes.sh functions ...
  set_mame_config_value "$SWITCHRES_INI" monitor custom
  set_mame_config_value "$SWITCHRES_INI" crt_range0 "$final_crt_range"
  log_ok "Set a new crt_range: $final_crt_range"

  # Lousy if MAME is not set to use the switchres.ini ...
  switchres_ini=$(get_mame_config_value switchres_ini)
  if [[ "$switchres_ini" != 1 ]] ; then
    ask_yesno "Set GroovyMAME to use your switchres.ini ?" && set_mame_config_value switchres_ini 1
  fi
}
