# DIALOG() taken from setup
# an el-cheapo dialog wrapper
#
# parameters: see dialog(1)
# returns: whatever dialog did
_dia_dialog() {
  dialog --backtitle "$TITLE" --aspect 15 "$@" 3>&1 1>&2 2>&3 3>&-
}

_dia_show_warning() {
  _dia_dialog --title "$1" --exit-label "Continue" --$3box "$2" 0 0 || die_error "dialog could not show --$3box $2. often this means a file does not exist"
}

_dia_notify() {
  _dia_dialog --msgbox "$@" 0 0
}

_dia_inform() {
  str="$1"
  if [ "$2" != 0 ]; then
    echo "$1" >>$LIBUI_DIA_SUCCESSIVE_ITEMS-$2
    str=$(cat $LIBUI_DIA_SUCCESSIVE_ITEMS-$2)
  fi
  [ "$3" = 1 ] && rm $LIBUI_DIA_SUCCESSIVE_ITEMS-$2
  _dia_dialog --infobox "$str" 0 0
}

_dia_ask_checklist() {
  str=$1
  elaborate=$2
  shift 2
  list=()
  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "no item given for element $1"
    [ -z "$3" ] && die_error "no ON/OFF switch given for element $1 (item $2)"
    [ "$3" != ON -a "$3" != OFF ] && die_error "element $1 (item $2) has status $3 instead of ON/OFF!"
    list+=("$1" "$2" $3)
    [ $elaborate -gt 0 ] && list+=("$4") # this can be an empty string, that's ok.
    shift 3
    [ $elaborate -gt 0 ] && shift
  done
  # i wish dialog would accept something like: --output-separator $'\0'
  # but it doesn't. there really is no good way to separate items currently
  # let's assume there are no newlines in the item tags
  ANSWER_CHECKLIST=()
  elab=''
  [ $elaborate -gt 0 ] && elab='--item-help'
  while read -r line; do
    ANSWER_CHECKLIST+=("$line")
  done < <(_dia_dialog --separate-output $elab --checklist "$str" 0 0 0 "${list[@]}")
  local ret=$?
  debug 'UI' "_dia_ask_checklist: user checked ON: ${ANSWER_CHECKLIST[@]}"
  return $ret
}

_dia_ask_datetime() {
  # display and ask to set date/time
  local _date _time
  _date=$(_dia_dialog --calendar "Set the date.\nUse <TAB> to navigate and arrow keys to change values." 0 0 0 0 0) || return 1 # form like: 07/12/2008
  _time=$(_dia_dialog --timebox "Set the time.\nUse <TAB> to navigate and up/down to change values." 0 0) || return 1           # form like: 15:26:46
  debug 'UI' "Date as specified by user $_date time: $_time"

  # DD/MM/YYYY hh:mm:ss -> MMDDhhmmYYYY.ss (date default format, set like date $ANSWER_DATETIME)  Not enabled because there is no use for it i think.
  # ANSWER_DATETIME=$(echo "$_date" "$_time" | sed 's#\(..\)/\(..\)/\(....\) \(..\):\(..\):\(..\)#\2\1\4\5\3\6#g')
  # DD/MM/YYYY hh:mm:ss -> YYYY-MM-DD hh:mm:ss ( date string format, set like date -s "$ANSWER_DATETIME")
  ANSWER_DATETIME="$(echo "$_date" "$_time" | sed 's#\(..\)/\(..\)/\(....\) \(..\):\(..\):\(..\)#\3-\2-\1 \4:\5:\6#g')"
}

_dia_ask_number() {
  #TODO: i'm not entirely sure this works perfectly. what if user doesnt give anything or wants to abort?
  while true; do
    str="$1"
    [ -n $2 ] && str2="min $2"
    [ -n $3 -a $3 != '0' ] && str2="$str2 max $3"
    [ -n "$str2" ] && str="$str ( $str2 )"
    ANSWER_NUMBER=$(_dia_dialog --inputbox "$str" 0 0 $4)
    local ret=$?
    if [[ $ANSWER_NUMBER == *[^0-9]* ]]; then #TODO: handle exit state
      show_warning 'Invalid number input' "$ANSWER_NUMBER is not a number! try again."
    else
      if [ -n "$3" ] && [ $3 != '0' -a $ANSWER_NUMBER -gt $3 ]; then
        show_warning 'Invalid number input' "$ANSWER_NUMBER is bigger then the maximum,$3! try again."
      elif [ -n "$2" ] && [ $ANSWER_NUMBER -lt $2 ]; then
        show_warning 'Invalid number input' "$ANSWER_NUMBER is smaller then the minimum,$2! try again."
      else
        break
      fi
    fi
  done
  debug 'UI' "_dia_ask_number: user entered: $ANSWER_NUMBER"
  [ -z "$ANSWER_NUMBER" ] && return 1
  return $ret
}

_dia_ask_option() {
  DEFAULT=""
  [ "$1" != 'no' ] && DEFAULT="--default-item $1"

  DIA_MENU_TITLE=$2
  EXTRA_INFO=$3
  shift 4
  CANCEL_LABEL=Cancel
  [ $TYPE == optional ] && CANCEL_LABEL='Skip'
  ANSWER_OPTION=$(_dia_dialog $DEFAULT --cancel-label $CANCEL_LABEL --colors --title " $DIA_MENU_TITLE " --menu "$LIBUI_DIA_MENU_TEXT $EXTRA_INFO" 0 0 0 "$@")
  local ret=$?
  debug 'UI' "dia_ask_option: ANSWER_OPTION: $ANSWER_OPTION, returncode (skip/cancel): $ret ($DIA_MENU_TITLE)"
  [ $TYPE == required ] && return $ret
  return 0 # TODO: check if dialog returned >0 because of an other reason then the user hitting 'cancel/skip'
}

_dia_ask_password() {
  if [ -n "$1" ]; then
    type_l=$(tr '[:upper:]' '[:lower:]' <<<$1)
    type_u=$(tr '[:lower:]' '[:upper:]' <<<$1)
  else
    type_l=
    type_u=
  fi

  local ANSWER=$(_dia_dialog --passwordbox "Enter your $type_l password" 8 65 "$2")
  local ret=$?
  [ -n "$type_u" ] && read ${type_u}_PASSWORD <<<$ANSWER
  [ -z "$type_u" ] && PASSWORD=$ANSWER
  echo $ANSWER
  debug 'UI' "_dia_ask_password: user entered <<hidden>>"
  return $ret
}

_dia_ask_string() {
  exitcode=${3:-1}
  ANSWER_STRING=$(_dia_dialog --title "$3" --inputbox "$1" 0 0 "$2")
  local ret=$?
  debug 'UI' "_dia_ask_string: user entered $ANSWER_STRING"
  [ -z "$ANSWER_STRING" ] && return $exitcode
  return $ret
}

_dia_ask_string_multiple() {
  MAXRESPONSE=0
  formtitle="$1"
  exitcode="${2:-1}"
  shift 2

  items=()
  line=1
  unset m
  i=0

  while [ -n "$1" ]; do
    [ -z "$2" ] && die_error "No default value for $1"
    # format: Label Y X Value Y X display-size value-size
    items+=("$1" $line 1 "$2" $line 20 20 $MAXRESPONSE)
    let line++
    shift 2
  done

  ANSWER_VALUES=()
  while read -r line; do
    ANSWER_VALUES+=("$line")
  done < <(_dia_dialog --form "$formtitle" 15 50 0 "${items[@]}")
}

_dia_ask_yesno() {
  local default
  str=$1
  # If $2 contains an explicit 'no' we set defaultno for yesno dialog
  [ "$2" == "no" ] && default="--defaultno"
  dialog $default --yesno "$str" 0 0 # returns 0 for yes, 1 for no
  local ret=$?
  [ $ret -eq 0 ] && debug 'UI' "dia_ask_yesno: User picked YES"
  [ $ret -gt 0 ] && debug 'UI' "dia_ask_yesno: User picked NO"
  return $ret
}

_dia_follow_progress() {
  title=$1
  logfile=$2

  _dia_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 >$LIBUI_FOLLOW_PID
  FOLLOW_PID=$(cat $LIBUI_FOLLOW_PID)
  rm $LIBUI_FOLLOW_PID

  # I wish something like this would work.  anyone who can explain me why it doesn't get's to be contributor of the month.
  # FOLLOW_PID=`_dia_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 2>&1 >/dev/null | head -n 1`

  # Also this doesn't work:
  # _dia_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0 &>/dev/null &
  # FOLLOW_PID=$!

  # Also the new stdout-stderr-swapping isn't a clean solution for that. When command substitition is used bash will
  # wait until the command terminates, dialog's forking to background will fail.
  # FOLLOW_PID=$(_dia_dialog --title "$1" --no-kill --tailboxbg "$2" 0 0)
}
