source ./zen.sh

export TITLE="Zenity testing suite"

_zen_show_warning "Warning title" "warning message"

_zen_notify "Notify text"

_zen_inform "Informing" 0

_zen_checklist

_zen_ask_datetime ; echo $ANSWER_DATETIME

_zen_ask_number "Set your age" 0 100 ; echo $ANSWER_NUMBER

_zen_ask_number "Input your age" ; echo $ANSWER_NUMBER

_zen_ask_option "soup" "title" '' 'required' 1 'mashed meat' 2 'soup' 3 'drink'

pass_app=ssh
user_pass=$(_zen_ask_password "$pass_app") ; echo "ANSWER: $user_pass / PASSWORD: $PASSWORD / ${pass_app}_PASSWORD: need to fix shell here"
user_pass=$(_zen_ask_password) ; echo "ANSWER: $user_pass / PASSWORD: $PASSWORD"

_zen_ask_string "Say your name:" "No One" ; echo $?

_zen_ask_string_multiple "Multiple entries" "" "text1" "default value 1" "text2" "default value 2" ; echo "${ANSWER_VALUES[@]}"
