#!/bin/bash

###### Set some default variables ######
TITLE="Groovy Arcade Linux Setup"
LOG="/dev/tty7"
LIB_CORE=/opt/gasetup/core
LIB_USER=/opt/gasetup/user
LIB_UIS="$LIB_CORE"/uis
RUNTIME_DIR=/tmp/gasetup
LOG_DIR=/var/log
LOGFILE=$LOG_DIR/gasetup.log
DISCLAIMER="This setup has the potential to remove disk drives and lose data, backup first."
export LC_COLLATE=C # for consistent sorting behavior

GAS_PATH=$(dirname "$(readlink -f $0)")
GAT_PATH=/opt/gatools/
GAT_INC="$GAT_PATH"/include
source "$GAT_INC"/includes.sh

###### Early bootstrap ######

# load the lib-ui, it is one we need everywhere so we must load it early.
source $LIB_CORE/libs/lib-ui.sh || {
  echo "Something went wrong while sourcing library $LIB_CORE/libs/lib-ui.sh" >&2
  exit 2
}
ui_init
# load the lib-flowcontrol. we also need some of it's functions early (like usage()).
source $LIB_CORE/libs/lib-flowcontrol.sh || {
  echo "Something went wrong while sourcing library $LIB_CORE/libs/lib-flowcontrol.sh" >&2
  exit 2
}
# lib-misc. we need it early, at least for check_is_in whis is used by the debug function.
source $LIB_CORE/libs/lib-misc.sh || {
  echo "Something went wrong while sourcing library $LIB_CORE/libs/lib-misc.sh" >&2
  exit 2
}


###### perform actual logic ######
echo "Welcome to $TITLE"

[[ $EUID -ne 0 ]] && die_error "You must have root privileges to run Groovy Arcade Setup"

# This is already done in worker_sysprep, should we really do that ?
findmnt -n -o OPTIONS / | tr -s ',' '\n' | grep -qE "^ro$" && mount -o remount,rw / &>/dev/null
cleanup_runtime
needed_pkgs_fs= # will hold extra needed packages for blockdevices/filesystems, populated when the Fs'es are created

### Set configuration values ###
# note : you're free to use or ignore these in your procedure.  probably you want to use these variables to override defaults in your configure worker

#DEBUG: don't touch it. it can be set in the env
arg_ui_type=
LOG_TO_FILE=0
module=
procedure=

# in that case -p needs to be the first option, but that's doable imho
# an alternative would be to provide an argumentstring for the profile. eg gasetup -p profile -a "-a a -b b -c c"

# you can override these variables in your procedures
var_OPTS_STRING=""
var_ARGS_USAGE=""

# Processes args that were not already matched by the basic rules.
process_args() {
  # known options: we don't know any yet
  # return 0

  # if we are still here, we didn't return 0 for a known option. hence this is an unknown option
  usage
  exit 5
}

# Check if the first args are -p <procedurename>.  If so, we can load the procedure, and hence $var_OPTS_STRING and process_args can be overridden
if [ "$1" = '-p' ]; then
  [ -z "$2" ] && usage && exit 1
  # note that we allow procedures like http://foo/bar. module -> http:, procedure -> http://foo/bar.
  if [[ $2 =~ ^http:// ]]; then
    module=http
    procedure="$2"
  elif grep -q '/' <<<"$2"; then
    #user specified module/procedure
    module=$(dirname "$2")
    procedure=$(basename "$2")
  else
    module=core
    procedure="$2"
  fi

  shift 2
fi

# If no procedure given, bail out
[ -z "$procedure" ] && usage && exit 5

load_module core
[ "$module" != core -a "$module" != http ] && load_module "$module"
# this is a workaround for bash <4.2, where associative arrays are inherently local,
# so we must source these variables in the main scope
source $LIB_CORE/libs/lib-blockdevices-filesystems.sh

load_procedure "$module" "$procedure"

while getopts ":i:cnf:u:dlp:$var_OPTS_STRING" OPTION; do
  case $OPTION in
    i)
      [ -z "$OPTARG" ] && usage && exit 1 #TODO: check if it's necessary to do this. the ':' in $var_OPTS_STRING might be enough
      [[ ! "cli dia zen" =~ "$OPTARG" ]] && die_error "-i must be dia zen or cli"
      arg_ui_type=$OPTARG
      ui_init
      ;;
    c)
      configure=1
      forced_package=
      eval nextopt=\${$OPTIND}
      # existing or starting with dash?
      if [[ -n $nextopt && $nextopt != -* ]] ; then
        OPTIND=$((OPTIND + 1))
        forced_package="$nextopt"
      fi
      ;;
    n)
      update=1
      forced_package=$OPTARG
      eval nextopt=\${$OPTIND}
      # existing or starting with dash?
      if [[ -n $nextopt && $nextopt != -* ]] ; then
        package_version_before="$nextopt"
        OPTIND=$((OPTIND + 1))
        eval nextopt=\${$OPTIND}
        package_version_after="$nextopt"
      fi
      ;;
    u)
      forced_user="$OPTARG"
      ;;
    f)
      # Call a specific function from gasetup w/o starting the UI
      # Implies -- for arguments passed to the function
      called_function="$OPTARG"
      args_from_index=$OPTIND
      ;;
    d)
      export DEBUG=1
      LOG_TO_FILE=1
      ;;
    l)
      LOG_TO_FILE=1
      ;;
    p)
      die_error "If you pass -p <procedurename>, it must be the FIRST option"
      ;;
    h)
      usage
      exit
      ;;
    ?)
      # If we hit something elso, call process_args
      process_args -$OPTION $OPTARG # you can override this function in your profile to parse additional arguments and/or override the behavior above
      ;;
  esac
done

if [[ -n $forced_user ]] ; then
  GA_USER="$forced_user"
else
  GA_USER="$SUDO_USER"
fi
export GA_USER

# If root is running the script, it should have a user specified
if [[ -z $SUDO_USER && -z  $GA_USER ]] ; then
  die_error "If you run this script as root not through sudo, you must specify a user."
fi

export GA_HOME="$(getent passwd "$GA_USER" | cut -d: -f6)"
export GA_GROUP="$(getent group "$GA_USER" | cut -d: -f1)"

# Set pacman vars.  allow procedures to have set $var_TARGET_DIR (TODO: look up how delayed variable substitution works. then we can put this at the top again)
# flags like --noconfirm should not be specified here.  it's up to the procedure to decide the interactivity
# NOTE: Pacman will run with currently active locale, if you want to parse output, you should prefix with LANG=C
PACMAN=pacman
PACMAN_TARGET="pacman --root $var_TARGET_DIR --config /tmp/pacman.conf"

# Do we want to configure ?
if [[ -n ${forced_package+x} ]] ; then
  # Let's go for configuring GA_USER for GroovyARCADE
  if [[ -n $configure ]] ; then
    log_info "Configure $forced_package"
    configure_package "$forced_package"
  elif [[ -n $update ]] ; then
    log_info "Update $forced_package"
    update_single_package "$forced_package" "$package_version_before" "$package_version_after"
  else
    die_error "Package is nor set to configure or update, abort"
  fi
elif [[ -n $called_function ]] ; then
  [[ $(type -t $called_function) == function ]] || die_error "$called_function is not a valid function"
  # call the corresponding procedure
  shift $((args_from_index - 1))
  $called_function "$@"
else
  start_installer
  start_process
  stop_installer
fi
