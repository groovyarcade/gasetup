#!/bin/bash
# TODO: implement 'retry until user does it correctly' everywhere
# TODO: at some places we should check if $1 etc is only 1 word because we often depend on that
# TODO: standardize. eg everything $1= question/title, $2=default. also default should probably be 'no' for no default everywhere
# TODO: figure out something to make dia windows always big enough, yet fit nicely in the terminal
# TODO: don't enforce default values in ask_string* functions, and maybe others

# you can call this function to change settings, before calling other libui functions or afterwards
# it must always be called at least once to set the right variables, but it gets automatically called once at the end of this file
# Don't set the variable or leave it empty to set the default value
# $1 ui type (dia or cli). default: cli
# $2 directory for tmp files. default: /tmp
# $3 logfile (or string of logfiles, separated by whitespace). default: no logging
# $4 array of categories you will use in debug calls. (useful when grepping logfiles). default: no debugging
# this library uses the UI debug category internally, you don't need to specify it. we add it automatically
libui_sh_init() {
  #echo $1
  LIBUI_UI=${1:-cli}
  allowed_uis=('cli' 'dia' 'zen')
  check_is_in "$LIBUI_UI" "${allowed_uis[@]}" || die_error "libui_sh_init \$1 must be one of 'cli', 'dia', 'zen' or '' (for cli)"
  [ "$LIBUI_UI" == 'dia' ] && ! which dialog &>/dev/null && die_error "Required dependency dialog not found"
  [ "$LIBUI_UI" == 'zen' ] && ! which zenity &>/dev/null && die_error "Required dependency zenity not found"
  [[ ! -f "$LIB_UIS"/"$LIBUI_UI".sh ]] && die_error "Couldn't load ui $LIB_UI source file"
  source "$LIB_UIS"/"$LIBUI_UI".sh
  LIBUI_TMP_DIR=/tmp
  if [ -n "$2" ]; then
    LIBUI_TMP_DIR=$2
    mkdir -p $LIBUI_TMP_DIR 2>/dev/null
  fi
  LIBUI_LOG=0
  if [ -n "$3" ]; then
    LIBUI_LOG=1
    LIBUI_LOG_FILE="$3"
  fi
  LIBUI_DEBUG=0
  shift 3
  if [ -n "$1" ]; then
    LIBUI_DEBUG=1
    LIBUI_DEBUG_CATEGORIES=("$@")
    check_is_in 'UI' "${LIBUI_DEBUG_CATEGORIES[@]}" || LIBUI_DEBUG_CATEGORIES+=('UI')
  fi
  LIBUI_DIA_SUCCESSIVE_ITEMS=$LIBUI_TMP_DIR/libui-sh-dia-successive-items
  LIBUI_FOLLOW_PID=$LIBUI_TMP_DIR/libui-sh-follow-pid
  LIBUI_DIA_MENU_TEXT="\n"
  #"Use the UP and DOWN arrows to navigate menus.  Use TAB to switch between buttons and ENTER to select."
}

## Helper functions ##
# $1 needle
# $2 set (array) haystack
check_is_in() {
  [ -z "$1" ] && die_error "check_is_in needs a non-empty needle as \$1 and a haystack as \$2!(got: check_is_in '$1' '$2'" # haystack can be empty though

  local needle="$1" element
  shift
  for element; do
    [[ $element == $needle ]] && return 0
  done
  return 1
}

# prompts user to choose an editor, and exports $EDITOR
# the list of choices is automatically adapted based on locally available editors
# if $EDITOR is non-empty and found, we return right away, unless $1 is 'force'
seteditor() {
  default=nano
  if [ -n "$EDITOR" ] && which "$EDITOR" >/dev/null; then
    [ "$1" != 'force' ] && return 0
    default=$EDITOR
  fi
  local opts=()
  declare -A editors=(["nano"]="nano (easy)" ["pico"]="pico (easy)" ["joe"]="joe (bit more powerful)" ["vi"]="vi (advanced)" ["vim"]="vim (advanced)")
  for editor in ${!editors[@]}; do
    which $editor &>/dev/null && opts+=($editor "${editors[$editor]}")
  done
  ask_option $default "Text editor selection" "Select a Text Editor to Use" required "${opts[@]}" || return 1
  check_is_in "$ANSWER_OPTION" "${!editors[@]}" && EDITOR=$ANSWER_OPTION || EDITOR=$default
  export EDITOR
}

### Functions that your code can use. Cli/dialog mode is fully transparant.  This library takes care of it ###
# you could write your own functions implementing other things (kdedialogs, zenity dialogs, ..),
# - just implement the functions below
# - source your library
# - libui-sh-init with the correct UI type

# display error message and die
# Do not call other functions like debug, notify, .. here because that might cause loops!
die_error() {
  echo -e "ERROR: $@" >&2
  exit 2
}

# display warning message
# $1 title
# $2 item to show
# $3 type of item.  msg or text if it's a file. (optional. defaults to msg)
show_warning() {
  [ -z "$1" ] && die_error "show_warning needs a title"
  [ -z "$2" ] && die_error "show_warning needs an item to show"
  [ -n "$3" -a "$3" != msg -a "$3" != text ] && die_error "show_warning \$3 must be text or msg"
  type=msg
  [ -n "$3" ] && type=$3
  debug 'UI' "show_warning '$1': $2 ($type)"
  [ $(type -t _${LIBUI_UI}_show_warning) == function ] || die_error "_${LIBUI_UI}_show_warning is not a function"
  _${LIBUI_UI}_show_warning "$1" "$2" $type
}

#notify user
notify() {
  debug 'UI' "notify: $@"
  [ $(type -t _${LIBUI_UI}_notify) == function ] || die_error "_${LIBUI_UI}_notify is not a function"
  _${LIBUI_UI}_notify "$@"
}

# like notify, but user does not need to confirm explicitly when in dia mode
# $1 str
# $2 0/<listname> this inform call is part of a successive list of things (eg repeat previous things, keep adding items to a list) (only needed for dia, cli does this by design).
#   You can keep several "lists of successive things" by grouping them with <listname>
#   this is somewhat similar to follow_progress.  Note that this list isn't cleared unless you set $3 to 1.  default 0. (optional).
# $3 0/1 this is the last one of the group of several things (eg clear buffer).  default 0. (optional)
inform() { #TODO: when using successive things, the screen can become full and you only see the old stuff, not the new
  successive=${2:-0}
  succ_last=${3:-0}
  debug 'UI' "inform: $1"
  [ $(type -t _${LIBUI_UI}_inform) == function ] || die_error "_${LIBUI_UI}_inform is not a function"
  _${LIBUI_UI}_inform "$1" $successive $succ_last
}

# logging of stuff
log() {
  [ "$LIBUI_LOG" = 1 ] || return
  for file in $LIBUI_LOG_FILE; do
    [ -z "$file" ] && continue
    dir=$(dirname $file)
    mkdir -p $dir || die_error "Cannot create log directory $dir"
    str="[LOG] $(date +"%Y-%m-%d %H:%M:%S") $@"
    echo -e "$str" >>$file || die_error "Cannot log $str to $file"
  done
}

# $1 = one or more debug categories (must be in a list of specified categories. see init function) (separated by spaces)
#      Useful when grepping in the logfile
# $2 = string to log
debug() {
  [ "$LIBUI_DEBUG" = "1" ] || return
  [ -n "$1" ] || die error "you must specify at least one (non-empty) debug category"
  [ -n "$2" ] || die_error "debug \$2 cannot be empty"
  for cat in $1; do
    check_is_in $cat "${LIBUI_DEBUG_CATEGORIES[@]}" || die_error "debug \$1 contains a value ($cat) which is not a valid debug category"
  done
  for file in $LIBUI_LOG_FILE; do
    [ -z "$file" ] && continue
    dir=$(dirname $file)
    mkdir -p $dir || die_error "Cannot create log directory $dir"
    str="[DEBUG $1 ] $2"
    echo -e "$str" >>$file || die_error "Cannot debug $str to $file"
  done
}

# ask for a timezone.
# this is pretty similar to how tzselect looks, but we support dia+cli + we don't actually change the clock + we don't show a date/time and ask whether it's okay. that comes later.
ask_timezone() {
  REGIONS=""
  for i in $(grep '^[A-Z]' /usr/share/zoneinfo/zone.tab | cut -f 3 | sed -e 's#/.*##g' | sort -u); do
    REGIONS="$REGIONS $i -"
  done
  while true; do
    ask_option no "Please select a region" '' required $REGIONS || return 1
    region=$ANSWER_OPTION
    ZONES=""
    for i in $(grep '^[A-Z]' /usr/share/zoneinfo/zone.tab | grep $region/ | cut -f 3 | sed -e "s#$region/##g" | sort -u); do
      ZONES="$ZONES $i -"
    done
    ask_option no "Please select a timezone" '' required $ZONES || return 1
    zone=$ANSWER_OPTION
    ANSWER_TIMEZONE="$region/$zone" && return
  done
}

# ask the user to make a selection from a certain group of things
# $1 question
# $2 whether each option gets an extra field for elaborate explanation: 1/0
# shift 2; $@ list of options, where each option is: $tag $item ON|OFF [elaborate_expl]
# if the tag suffices for you, you can set item to ^ for ON items and - for OFF items (for some visual indication). these items will not be shown in cli mode.
# tags may not contain newlines because we use that as output separator. (due to dialog limitation). quotes are ok.
# the extra explanation field may not contain newlines, and \n are not recognized in dia mode. (dialog limitation), in cli mode this is ok
# gives you $ANSWER_CHECKLIST, an array containing all tags
ask_checklist() {
  [ -z "$1" ] && die_error "ask_checklist needs a question!"
  [ "$2" != 0 -a "$2" != 1 ] && die_error "ask_checklist param 2 needs to be 0/1 to denote whether there's an elaborate explanation field per option"
  [ -z "$5" ] && debug 'UI' "ask_checklist args: $@" && die_error "ask_checklist makes only sense if you specify at least 1 thing (tag,item and ON/OFF switch)"
  [ $(type -t _${LIBUI_UI}_ask_checklist) == function ] || die_error "_${LIBUI_UI}_ask_checklist is not a function"
  _${LIBUI_UI}_ask_checklist "$@"
}

ask_datetime() {
  [ $(type -t _${LIBUI_UI}_ask_datetime) == function ] || die_error "_${LIBUI_UI}_ask_datetime is not a function"
  _${LIBUI_UI}_ask_datetime "$@"
}

# ask for a number.
# $1 question
# $2 lower limit (optional)
# $3 upper limit (optional. set 0 for none)
# $4 default (optional)
# sets $ANSWER_NUMBER to the number the user specified
# returns 1 if the user cancelled or did not enter a numeric, 0 otherwise
ask_number() {
  [ -z "$1" ] && die_error "ask_number needs a question!"
  [ -n "$2" ] && [[ "$2" == *[^0-9]* ]] && die_error "ask_number \$2 must be a number! not $2"
  [ -n "$3" ] && [[ "$3" == *[^0-9]* ]] && die_error "ask_number \$3 must be a number! not $3"
  [ -n "$4" ] && [[ "$4" == *[^0-9]* ]] && die_error "ask_number \$4 must be a number! not $4"
  [ $(type -t _${LIBUI_UI}_ask_number) == function ] || die_error "_${LIBUI_UI}_ask_number is not a function"
  _${LIBUI_UI}_ask_number "$1" $2 $3 $4
}

# ask the user to choose something
# $1 default item (set to 'no' for none)
# $2 title
# $3 additional explanation (default: '')
# $4 type (required or optional). '' means required. cancel labels will be 'Cancel' and 'Skip' respectively.
# shift 4 ; $@ list of options. first tag. then name. (eg tagA itemA "tag B" 'item B' )

# $ANSWER_OPTION : selected answer (if none selected: default (if available), or empty string otherwise). if user hits cancel or skip, this is an empty string.
# $?             : 0 if the user selected anything or skipped (when optional), when user cancelled: 1
ask_option() {
  [ $(type -t _${LIBUI_UI}_ask_option) == function ] || die_error "_${LIBUI_UI}_ask_option is not a function"
  [ -z "$1" ] && debug 'UI' "ask_option args: $@" && die_error "ask_option \$1 must be the default item (or 'no' for none)"
  [ -z "$2" ] && debug 'UI' "ask_option args: $@" && die_error "ask_option \$2 must be the title"
  TYPE=${4:-required}
  [ "$TYPE" != required -a "$TYPE" != optional ] && debug 'UI' "ask_option args: $@" && die_error "ask option \$4 must be required or optional or ''. not $TYPE"
  [ -z "$6" ] && debug 'UI' "ask_option args: $@" && die_error "ask_option makes only sense if you specify at least one option (with tag and name)" #nothing wrong with only 1 option.  it still shows useful info to the user
  _${LIBUI_UI}_ask_option "$@"
}

# ask the user a password. return is stored in $PASSWORD or $<TYPE>_PASSWORD
# $1 type (optional.  eg 'svn', 'ssh').
ask_password() {
  [ $(type -t _${LIBUI_UI}_ask_password) == function ] || die_error "_${LIBUI_UI}_ask_pasword is not a function"
  _${LIBUI_UI}_ask_password "$@"
}

# ask for a string.
# $1 question
# $2 default (optional)
# $3 exitcode to use when string is empty and there was no default, or default was ignored (1 default)
# Sets $ANSWER_STRING to response.
# returns 1 if the user cancelled, 0 otherwise
ask_string() {
  [ -z "$1" ] && die_error "ask_string needs a question!"
  [ $(type -t _${LIBUI_UI}_ask_string) == function ] || die_error "_${LIBUI_UI}_ask_string is not a function"
  _${LIBUI_UI}_ask_string "$1" "$2" "$3"
}

# ask multiple questions te get multiple string answers (form).
# $1 question/title
# $2 exitcode to use when one of the strings is empty and there was no default, or default was ignored (1 default) (ignored in dia mode)
# [ $3 questionlabel, $4 questiondefaultvalue , .. ]
# returns 1 if the user cancelled, 0 otherwise
# populates $ANSWER_VALUES array, indexed by question number starting at 0.
ask_string_multiple() {
  [ -z "$1" ] && die_error "ask_string_multiple needs a question!"
  [ $(type -t _${LIBUI_UI}_ask_string_multiple) == function ] || die_error "_${LIBUI_UI}_ask_string_multiple is not a function"
  _${LIBUI_UI}_ask_string_multiple "$@"
}

# ask a yes/no question.
# $1 question
# $2 default answer yes/no (optional)
# returns 0 if response is yes/y (case insensitive).  1 otherwise
ask_yesno() {
  [ -z "$1" ] && die_error "ask_yesno needs a question!"
  [ $(type -t _${LIBUI_UI}_ask_yesno) == function ] || die_error "_${LIBUI_UI}_ask_yesno is not a function"
  _${LIBUI_UI}_ask_yesno "$@"
}

# follow the progress of something by showing it's log, updating real-time
# $1 title
# $2 logfile
# $3 pid to monitor. if process stopped, stop following (only used in cli mode)
follow_progress() {
  [ -z "$1" ] && die_error "follow_progress needs a title!"
  [ -z "$2" ] && die_error "follow_progress needs a logfile to follow!"
  FOLLOW_PID=
  [ $(type -t _${LIBUI_UI}_follow_progress) == function ] || die_error "_${LIBUI_UI}_follow_progress is not a function"
  _${LIBUI_UI}_follow_progress "$@"
}

libui_sh_init
